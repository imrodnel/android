package fr.istic.masteru.android.utils;

public class API_URLS {

    public static final String AUTH_TOKEN = "ac_token";
    public static final String BASE_URL = "http://5.196.21.161:22222";

    public static final String PING_ADD_NUMBER = BASE_URL + "/test/nombres/ajouterNombre";
    public static final String SCORE_UPDATE = BASE_URL + "/score/update";
    public static final String MATCH_WINNER = BASE_URL + "/match/finMatch";
    public static final String MATCHS = BASE_URL + "/matches";
    public static final String AVAILABLE_COURTS = BASE_URL + "/courts/available";
    public static final String LOGIN = BASE_URL + "/login";

    public static String TEAM_PLAYERS(String teamId, String gender) {
        return BASE_URL + "/teams/" + teamId + "/players?gender=" + (gender == null ? "X" : gender);
    }

    public static String INFO_MATCH(int matchId) {
        return BASE_URL + "/matches/" + matchId;
    }

    public static String UPDATE_MATCH(int matchId) {
        return BASE_URL + "/matches/" + matchId;
    }

    public static String UPDATE_SCORE(int matchId, int pointId) {
        return BASE_URL + "/matches/" + matchId + "/score/" + pointId;
    }

    public static String UPDATE_SET_SCORE(int matchId, int setId) {
        return BASE_URL + "/matches/" + matchId + "/set/" + setId;
    }

    public static String DELETE_SCORE(int matchId, int pointId) {
        return BASE_URL + "/matches/" + matchId + "/score/" + pointId;
    }

}
