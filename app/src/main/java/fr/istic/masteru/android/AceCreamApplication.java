package fr.istic.masteru.android;

import android.app.Application;

import fr.istic.masteru.android.utils.FontUtils;

public class AceCreamApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontUtils.loadFont(getApplicationContext());
    }
}
