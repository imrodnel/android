package fr.istic.masteru.android.utils;

import android.content.Context;

public class PrefsUtils {

    private static final String PREFS_NAME = "AceCreamPrefs";
    private static final String TOKEN = "TOKEN";

    public static void setToken(Context context, String token) {
        context.getSharedPreferences(PREFS_NAME, 0).edit().putString(TOKEN, token).commit();
    }

    public static String getToken(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getString(TOKEN, null);
    }
}
