package fr.istic.masteru.android.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.automate.Automate;
import fr.istic.masteru.android.automate.FileManager;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.model.TeamInfo;
import fr.istic.masteru.android.utils.API_URLS;
import fr.istic.masteru.android.utils.OnFinishCallback;
import fr.istic.masteru.android.utils.Utils;

public class HomeActivity extends Activity {

    private Button resumeMatchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        findViewById(R.id.startMatchButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, MatchSetupActivity.class);
                startActivity(intent);
            }
        });
        resumeMatchButton = (Button) findViewById(R.id.resumeMatchButton);
        resumeMatchButton.setEnabled(false);
        checkForSavedAutomate();
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Authentification...");
        dialog.show();
        Utils.getToken(this, new OnFinishCallback() {
            @Override
            public void onFinish(Object object) {
                dialog.cancel();
            }
        });
    }

    private void checkForSavedAutomate() {
        if (FileManager.fileExist(this)) {
            new AsyncTask<Void, Void, Automate>() {

                @Override
                protected Automate doInBackground(Void... voids) {
                    return FileManager.importAutomate(HomeActivity.this);
                }

                @Override
                protected void onPostExecute(final Automate automate) {
                    super.onPostExecute(automate);
                    Ion.with(HomeActivity.this)
                            .load("GET", API_URLS.INFO_MATCH(automate.getIdMatch()))
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    if (e != null || result == null) {
                                        checkForSavedAutomate();
                                        return;
                                    }
                                    JsonObject jsonMatch = result.getAsJsonObject("match");

                                    String status = jsonMatch.get("Statut").getAsString();
                                    if (!status.equals("PROGRESS"))
                                        return;

                                    String isoA = jsonMatch.get("Team_A").getAsString();
                                    String isoB = jsonMatch.get("Team_B").getAsString();
                                    String teamNameA = Utils.getStringFromJson(jsonMatch, "NameTeam_A");
                                    String teamNameB = Utils.getStringFromJson(jsonMatch, "NameTeam_B");
                                    String flagA = Utils.getStringFromJson(jsonMatch, "UrlTeam_A");
                                    String flagB = Utils.getStringFromJson(jsonMatch, "UrlTeam_B");
                                    TeamInfo teamA = new TeamInfo(isoA, teamNameA, flagA);
                                    TeamInfo teamB = new TeamInfo(isoB, teamNameB, flagB);

                                    final String category = jsonMatch.getAsJsonPrimitive("Category").getAsString();
                                    String tableau = jsonMatch.getAsJsonPrimitive("Tableau").getAsString();

                                    final PlayerInfo playerA1, playerA2, playerB1, playerB2;
                                    int idPlayerA1 = jsonMatch.getAsJsonPrimitive("IdPlayerA_1").getAsInt();
                                    int idPlayerB1 = jsonMatch.getAsJsonPrimitive("IdPlayerB_1").getAsInt();
                                    String nameA1 = Utils.getStringFromJson(jsonMatch, "Player_A1");
                                    String nameB1 = Utils.getStringFromJson(jsonMatch, "Player_B1");
                                    String photoUrlA1 = Utils.getStringFromJson(jsonMatch, "UrlA_1");
                                    String photoUrlB1 = Utils.getStringFromJson(jsonMatch, "UrlB_1");
                                    playerA1 = new PlayerInfo(idPlayerA1, PlayerInfo.MAN, nameA1, photoUrlA1, teamA);
                                    playerB1 = new PlayerInfo(idPlayerB1, PlayerInfo.MAN, nameB1, photoUrlB1, teamB);
                                    if (category.contains("D")) {
                                        int idPlayerA2 = jsonMatch.getAsJsonPrimitive("IdPlayerA_2").getAsInt();
                                        int idPlayerB2 = jsonMatch.getAsJsonPrimitive("IdPlayerB_2").getAsInt();
                                        String nameA2 = Utils.getStringFromJson(jsonMatch, "Player_A2");
                                        String nameB2 = Utils.getStringFromJson(jsonMatch, "Player_B2");
                                        String photoUrlA2 = Utils.getStringFromJson(jsonMatch, "UrlA_2");
                                        String photoUrlB2 = Utils.getStringFromJson(jsonMatch, "UrlB_2");
                                        playerA2 = new PlayerInfo(idPlayerA2, PlayerInfo.MAN, nameA2, photoUrlA2, teamA);
                                        playerB2 = new PlayerInfo(idPlayerB2, PlayerInfo.MAN, nameB2, photoUrlB2, teamB);
                                    } else {
                                        playerA2 = null;
                                        playerB2 = null;
                                    }
                                    final int court = jsonMatch.get("Court").getAsInt();
                                    final MatchInfo match = new MatchInfo(automate.getIdMatch(), teamA, teamB, category, tableau);
                                    resumeMatchButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (category.contains("D")) {
                                                Intent intent = new Intent(HomeActivity.this, DoubleMatchActivity.class);
                                                intent.putExtra(MatchActivity.RESUME_MATCH, true);
                                                intent.putExtra(MatchActivity.MATCH, match);
                                                intent.putExtra(MatchActivity.COURT_NUM, court);
                                                intent.putExtra(MatchActivity.PLAYER_A1, playerA1);
                                                intent.putExtra(MatchActivity.PLAYER_A2, playerA2);
                                                intent.putExtra(MatchActivity.PLAYER_B1, playerB1);
                                                intent.putExtra(MatchActivity.PLAYER_B2, playerB2);
                                                startActivity(intent);
                                            } else {
                                                Intent intent = new Intent(HomeActivity.this, SingleMatchActivity.class);
                                                intent.putExtra(MatchActivity.RESUME_MATCH, true);
                                                intent.putExtra(MatchActivity.MATCH, match);
                                                intent.putExtra(MatchActivity.COURT_NUM, court);
                                                intent.putExtra(MatchActivity.PLAYER_A1, playerA1);
                                                intent.putExtra(MatchActivity.PLAYER_B1, playerB1);
                                                startActivity(intent);
                                            }
                                        }
                                    });
                                    resumeMatchButton.setText("Reprendre le match " + teamNameA + " vs " + teamNameB + " (" + Utils.getCategorieName(category) + ")");
                                    resumeMatchButton.setEnabled(true);
                                }
                            });
                }
            }.execute(null, null, null);
        }
    }

}
