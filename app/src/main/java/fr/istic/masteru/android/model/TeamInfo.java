package fr.istic.masteru.android.model;

import java.io.Serializable;

public class TeamInfo implements Serializable {

    private String ISO;
    private String name;
    private String flagUrl;

    public TeamInfo(String ISO, String name, String flagUrl) {
        this.ISO = ISO;
        this.name = name;
        this.flagUrl = flagUrl;
    }

    public String getISO() {
        return ISO;
    }

    public void setISO(String ISO) {
        this.ISO = ISO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }
}
