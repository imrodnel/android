package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.model.TeamInfo;
import fr.istic.masteru.android.utils.Constants;
import fr.istic.masteru.android.utils.Utils;

public class ResultFragment extends Fragment {

    MatchSetupActivity matchSetupActivity;
    TextView courtTV;
    TextView categorieTV;
    TextView tableauTV;
    TextView refereeingTypeTV;
    ImageView flagA, flagB;
    TextView countryATV, countryBTV;
    TextView playerA1TV, playerA2TV, playerB1TV, playerB2TV;
    TextView firstService;
    TextView sideA, sideB;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.result_fragment, container, false);
        courtTV = (TextView) root.findViewById(R.id.court);
        categorieTV = (TextView) root.findViewById(R.id.categorie);
        tableauTV = (TextView) root.findViewById(R.id.tableau);
        refereeingTypeTV = (TextView) root.findViewById(R.id.refereeingType);
        flagA = (ImageView) root.findViewById(R.id.flagA);
        flagB = (ImageView) root.findViewById(R.id.flagB);
        countryATV = (TextView) root.findViewById(R.id.countryA);
        countryBTV = (TextView) root.findViewById(R.id.countryB);
        playerA1TV = (TextView) root.findViewById(R.id.playerA1);
        playerA2TV = (TextView) root.findViewById(R.id.playerA2);
        playerB1TV = (TextView) root.findViewById(R.id.playerB1);
        playerB2TV = (TextView) root.findViewById(R.id.playerB2);
        firstService = (TextView) root.findViewById(R.id.fsfButton);
        sideA = (TextView) root.findViewById(R.id.sideA);
        sideB = (TextView) root.findViewById(R.id.sideB);
        return root;
    }

    public void setMatchInfos(MatchInfo match) {
        if (match == null) {
            categorieTV.setText(null);
            tableauTV.setText(null);
            flagA.setImageBitmap(null);
            flagB.setImageBitmap(null);
            countryATV.setText(null);
            countryBTV.setText(null);
        } else {
            if (match.getCategory().equals("SM")) {
                int num = match.getId() % 7;
                categorieTV.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
            } else if (match.getCategory().equals("SW")) {
                int num = (match.getId() - 2) % 7;
                categorieTV.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
            } else {
                categorieTV.setText(Utils.getCategorieName(match.getCategory()));
            }
            tableauTV.setText(Utils.getTableauName(match.getTableau()));
            Ion.with(flagA)
                    .load(match.getTeamA().getFlagUrl());
            Ion.with(flagB)
                    .load(match.getTeamB().getFlagUrl());
            countryATV.setText(match.getTeamA().getName());
            countryBTV.setText(match.getTeamB().getName());
        }
    }

    public void setCategorie(String categorie) {
        categorieTV.setText(Utils.getCategorieName(categorie));
    }

    public void setRefereeingType(int type) {
        if (type == 0) {
            refereeingTypeTV.setVisibility(View.GONE);
        } else {
            refereeingTypeTV.setText(Utils.getRefereeingTypeName(type));
            refereeingTypeTV.setVisibility(View.VISIBLE);
        }
    }

    public void setPlayerA1(PlayerInfo playerA1) {
        if (playerA1 == null)
            playerA1TV.setText(null);
        else
            playerA1TV.setText(playerA1.getName());
    }

    public void setPlayerA2(PlayerInfo playerA2) {
        if (playerA2 == null)
            playerA2TV.setText(null);
        else
            playerA2TV.setText(playerA2.getName());
    }

    public void setPlayerB1(PlayerInfo playerB1) {
        if (playerB1 == null)
            playerB1TV.setText(null);
        else
            playerB1TV.setText(playerB1.getName());
    }

    public void setPlayerB2(PlayerInfo playerB2) {
        if (playerB2 == null)
            playerB2TV.setText(null);
        else
            playerB2TV.setText(playerB2.getName());
    }

    public void setFirstService(TeamInfo teamInfo) {
        if (teamInfo == null) {
            firstService.setText(null);
        } else {
            firstService.setText("First service: " + teamInfo.getName());
        }
    }

    public void setServiceSide(int side) {
        if (side == Constants.A_IS_LEFT) {
            sideA.setText("(Left)");
            sideB.setText("(Right)");
        } else if (side == Constants.A_IS_RIGHT) {
            sideA.setText("(Right)");
            sideB.setText("(Left)");
        } else {
            sideA.setText(null);
            sideB.setText(null);
        }
    }

    public void setCourt(int courtId) {
        if (courtId == Constants.UNDEFINED) {
            courtTV.setText(null);
        } else {
            courtTV.setText(Utils.getCourtName(courtId));
        }
    }
}
