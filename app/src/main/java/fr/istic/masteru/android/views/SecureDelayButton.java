package fr.istic.masteru.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import java.util.Timer;
import java.util.TimerTask;

public class SecureDelayButton extends Button implements View.OnClickListener {

    private static final int DELAY_MS = 555;

    private OnClickListener onClickListener;
    private boolean securing;
    private boolean keepDisabled;

    public SecureDelayButton(Context context) {
        super(context);
        init();
    }

    public SecureDelayButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SecureDelayButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        super.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        securing = true;
        if (onClickListener != null)
            onClickListener.onClick(this);

        super.setEnabled(false);
        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                post(new Runnable() {
                    @Override
                    public void run() {
                        if (keepDisabled) {
                            keepDisabled = false;
                        } else {
                            SecureDelayButton.super.setEnabled(true);
                        }
                        securing = false;
                    }
                });
            }
        }, DELAY_MS);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (securing && !enabled)
            keepDisabled = true;
        else
            super.setEnabled(enabled);
    }

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        this.onClickListener = l;
    }
}
