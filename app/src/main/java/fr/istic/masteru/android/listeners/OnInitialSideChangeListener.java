package fr.istic.masteru.android.listeners;

public interface OnInitialSideChangeListener {
    public void onInitialSideChange(int side);
}
