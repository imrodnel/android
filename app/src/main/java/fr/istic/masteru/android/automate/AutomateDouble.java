package fr.istic.masteru.android.automate;

import android.content.Context;

import java.util.Stack;
import java.util.Vector;

/**
 * Created by Spyrit on 01/10/2014.
 */
public class AutomateDouble implements Automate {

    public Vector<Step> pile = new Vector<>();
    public Stack<Integer> pileEtat = new Stack<>();
    protected int etat;
    protected int numact;
    protected InfoScore score;
    private int idMatch;
    private Context context;

    public AutomateDouble(Context context, int service, boolean sideLeftA, int idMatch) {
        this(context, service, sideLeftA, idMatch, false);
        FileManager.createFile(context, 1, service, (sideLeftA ? 1 : 0), idMatch);
    }

    public AutomateDouble(Context context, int service, boolean sideLeftA, int idMatch, boolean isAlready) {
        this.context = context;
        this.idMatch = idMatch;
        int myInt = (sideLeftA) ? 1 : 0;
        myInt = myInt * 10;
        pile.add(new Step(DoublePoint.executer(-(service) - myInt).copy(), null));
        etat = 0;
        pileEtat.add(etat);
    }


    @Override
    public Point point(Stats stats) {
        Point point = pointWithoutSaving(stats);
        FileManager.point(context, point);
        return point;
    }

    @Override
    public Point pointWithoutSaving(Stats stats) {
        int code = stats.getWinner();
        /** Vérification TieBreak */
        if (!DoublePoint.getScore().isTieBreak()) {
            /** Vérification SuperTieBreak */
            if (!DoublePoint.getScore().isSuperTieBreak()) {
                numact = DoublePoint.action[etat][code];
                etat = DoublePoint.transit[etat][code];
                score = DoublePoint.executer(numact).copy();
            } else {
                score = DoublePoint.executer(20 + code).copy();
            }
        } else {
            score = DoublePoint.executer(10 + code).copy();
        }

        stats.setBreakPoint(scoreIsBreakState(score));

        Point point = new Point(score.getScore(), stats);

       /* Ajout de l'état dans une pile d'état */
        pileEtat.add(etat);

        /* Ajout de score dans un vecteur de score */
        pile.add(new Step(score, stats));
        return point;
    }

    private boolean scoreIsBreakState(InfoScore score) {
        // Une balle de break est présente si le relanceur à l'occasion de gagner le jeu

        // On récupère le score
        int scoreA, scoreB;
        scoreA = score.getScoreA();
        scoreB = score.getScoreB();

        // On récupère le relanceur
        int relanceur;
        relanceur = (score.getService() + 1) % 2;

        // Relanceur est joueur A
        if (relanceur == 0) {
            // On cherche la différence de score
            if ((scoreA > scoreB) && (scoreA == 40)) {
                return true;
            } else {
                return false;
            }
        }
        // Relanceur est joueur B
        else {
            // On cherche la différence de score
            if ((scoreB > scoreA) && (scoreB == 40)) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public Point getPoint(int index) {
        return pile.elementAt(index).getPoint();
    }

    @Override
    public Point back() {
        /* Delete si pile non "vide" */
        if (pile.size() > 1 && pile.size() > 1) {

            /* Suppression de l'élément dans la pile */
            pile.removeElementAt(pile.size() - 1);

           /* Pop du dernier état */
            pileEtat.pop();

            /* Mise à jour état */
            etat = pileEtat.lastElement();

            /* Mise à jour score */
            DoublePoint.setScore(pile.lastElement().getInfoScore());

            /* OLD : Vérification TieBreak & SuperTieBreak
            if (DoublePoint.getScore().getGamesA() == 6 && DoublePoint.getScore().getGamesB() == 6) {
                DoublePoint.tieBreak = true;
                DoublePoint.superTieBreak = false;
            } else if (DoublePoint.getScore().getSetA() == 1 && DoublePoint.getScore().getSetB() == 1) {
                DoublePoint.superTieBreak = true;
                DoublePoint.tieBreak = false;
            } else {
                DoublePoint.superTieBreak = false;
                DoublePoint.tieBreak = false;
            }
            */
        }
        FileManager.deleteLast(context);
        return pile.lastElement().getPoint();
    }

    public int getIdMatch() {
        return idMatch;
    }

    @Override
    public Point last() {
        return pile.lastElement().getPoint();
    }

    @Override
    public Point switchSide() {

        Stats stat = null;
        Score score1 = null;
        InfoScore infoScore;
        Point point = null;
        Step step = null;

        /* Modifier l'état du score de l'automate */
        DoublePoint.score.setSideLeftA(!DoublePoint.score.isSideLeftA());

        /* Modifier l'état dans le fichier et la pile */
        step = pile.get(pile.size() - 1);
        infoScore = step.getInfoScore().copy();
        if (step.getStats() != null) {
            stat = step.getStats().copy();
        }
        infoScore.setSideLeftA(!infoScore.isSideLeftA());

        step = new Step(infoScore, stat);

        pile.setElementAt(step, pile.size() - 1);

        point = new Point(infoScore.getScore(), stat);
        FileManager.deleteLast(context);
        FileManager.point(context, point);

        return point;
    }

    @Override
    public Point switchService() {

        Stats stat = null;
        Score score1 = null;
        InfoScore infoScore;
        Point point = null;
        Step step = null;

        /* Modifier l'état du score de l'automate */
        DoublePoint.score.setService((DoublePoint.score.getService() + 1) % 2);

        /* Modifier l'état dans le fichier et la pile */
        step = pile.get(pile.size() - 1);
        infoScore = step.getInfoScore().copy();
        if (step.getStats() != null) {
            stat = step.getStats().copy();
        }

        infoScore.setService((infoScore.getService() + 1) % 2);

        step = new Step(infoScore, stat);

        pile.setElementAt(step, pile.size() - 1);

        point = new Point(infoScore.getScore(), stat);
        FileManager.deleteLast(context);
        FileManager.point(context, point);

        return point;
    }
}
