package fr.istic.masteru.android.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.automate.Automate;
import fr.istic.masteru.android.automate.FileManager;
import fr.istic.masteru.android.automate.Point;
import fr.istic.masteru.android.automate.Score;
import fr.istic.masteru.android.automate.SimplePoint;
import fr.istic.masteru.android.automate.Stats;
import fr.istic.masteru.android.listeners.OnSyncFinishListener;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.utils.Constants;
import fr.istic.masteru.android.utils.ScoreSyncManager;
import fr.istic.masteru.android.utils.Utils;
import fr.istic.masteru.android.views.MatchScoreView;
import fr.istic.masteru.android.views.SecureDelayButton;

/**
 * Base class for Match management.
 * Subclass layout must contains viewsID present in the MatchActivity#onCreate
 */
public abstract class MatchActivity extends Activity {

    // Intent args
    public static final String MATCH = "MATCH";
    public static final String SIDE_LEFT_A = "SIDE_LEFT_A";
    public static final String SERVICE = "SERVICE";
    public static final String COURT_NUM = "COURT_NUM";
    public static final String PLAYER_A1 = "PLAYER_A1";
    public static final String PLAYER_A2 = "PLAYER_A2";
    public static final String PLAYER_B1 = "PLAYER_B1";
    public static final String PLAYER_B2 = "PLAYER_B2";
    public static final String RESUME_MATCH = "RESUME_MATCH";

    protected Automate automate;
    protected boolean isPlayerALeft;
    protected MatchScoreView matchScoreView;
    protected TextView scorePlayerA, scorePlayerB;
    protected MatchInfo match;
    private ImageView serviceLeft, serviceRight;
    private Button backButton;
    private TextView gameTime;

    private Point currentPoint;

    protected TextView playerLeft1Name, playerRight1Name;
    protected TextView playerLeft2Name, playerRight2Name;
    protected TextView playerLeftNat, playerRightNat;
    protected ImageView playerLeft1Photo, playerRight1Photo;
    protected ImageView playerLeft2Photo, playerRight2Photo;
    protected PlayerInfo playerInfoA1, playerInfoA2;
    protected PlayerInfo playerInfoB1, playerInfoB2;
    protected String nameA, nameB;

    protected Button fsfButton;
    protected View leftButtons, rightButtons;
    protected SecureDelayButton leftPointButton, rightPointButton; // "Normal" point are considered as Forced Error
    protected SecureDelayButton leftDoubleFaultButton, rightDoubleFaultButton;
    protected SecureDelayButton leftAceButton, rightAceButton;
    protected SecureDelayButton leftWinnerServeButton, rightWinnerServeButton;
    protected SecureDelayButton leftWinnerPointButton, rightWinnerPointButton;
    protected SecureDelayButton leftUnforcedErrorButton, rightUnforcedErrorButton;
    protected List<SecureDelayButton> animatedLeftButtons = new ArrayList<>();
    protected List<SecureDelayButton> animatedRightButtons = new ArrayList<>();

    private ScoreSyncManager syncManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.match_activity);
        match = (MatchInfo) getIntent().getSerializableExtra(MATCH);
        matchScoreView = (MatchScoreView) findViewById(R.id.matchScoreView);

        // Infos views
        playerInfoA1 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_A1);
        playerInfoA2 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_A2);
        playerInfoB1 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_B1);
        playerInfoB2 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_B2);

        playerLeft1Name = (TextView) findViewById(R.id.playerLeft1NameTV);
        playerLeft2Name = (TextView) findViewById(R.id.playerLeft2NameTV);
        playerRight1Name = (TextView) findViewById(R.id.playerRight1NameTV);
        playerRight2Name = (TextView) findViewById(R.id.playerRight2NameTV);
        playerLeftNat = (TextView) findViewById(R.id.playerLeftNatTV);
        playerRightNat = (TextView) findViewById(R.id.playerRightNatTV);
        playerLeft1Photo = (ImageView) findViewById(R.id.playerLeft1Photo);
        playerLeft2Photo = (ImageView) findViewById(R.id.playerLeft2Photo);
        playerRight1Photo = (ImageView) findViewById(R.id.playerRight1Photo);
        playerRight2Photo = (ImageView) findViewById(R.id.playerRight2Photo);

        serviceLeft = (ImageView) findViewById(R.id.serviceLeft);
        serviceRight = (ImageView) findViewById(R.id.serviceRight);
        gameTime = (TextView) findViewById(R.id.gameTime);
        backButton = (Button) findViewById(R.id.back);

        // Stats views
        fsfButton = (Button) findViewById(R.id.fsfButton);
        leftButtons = findViewById(R.id.leftButtons);
        rightButtons = findViewById(R.id.rightButtons);

        leftAceButton = (SecureDelayButton) findViewById(R.id.aceLeft);
        leftDoubleFaultButton = (SecureDelayButton) findViewById(R.id.doubleFaultLeft);
        leftPointButton = (SecureDelayButton) findViewById(R.id.pointLeft);
        leftWinnerServeButton = (SecureDelayButton) findViewById(R.id.winnerServeLeft);
        leftWinnerPointButton = (SecureDelayButton) findViewById(R.id.winnerPointLeft);
        leftUnforcedErrorButton = (SecureDelayButton) findViewById(R.id.unforcedErrorLeft);
        animatedLeftButtons.add(leftAceButton);
        animatedLeftButtons.add(leftDoubleFaultButton);
        animatedLeftButtons.add(leftPointButton);
        animatedLeftButtons.add(leftWinnerServeButton);
        animatedLeftButtons.add(leftWinnerPointButton);
        animatedLeftButtons.add(leftUnforcedErrorButton);

        rightAceButton = (SecureDelayButton) findViewById(R.id.aceRight);
        rightDoubleFaultButton = (SecureDelayButton) findViewById(R.id.doubleFaultRight);
        rightPointButton = (SecureDelayButton) findViewById(R.id.pointRight);
        rightWinnerServeButton = (SecureDelayButton) findViewById(R.id.winnerServeRight);
        rightWinnerPointButton = (SecureDelayButton) findViewById(R.id.winnerPointRight);
        rightUnforcedErrorButton = (SecureDelayButton) findViewById(R.id.unforcedErrorRight);
        animatedRightButtons.add(rightAceButton);
        animatedRightButtons.add(rightDoubleFaultButton);
        animatedRightButtons.add(rightPointButton);
        animatedRightButtons.add(rightWinnerServeButton);
        animatedRightButtons.add(rightWinnerPointButton);
        animatedRightButtons.add(rightUnforcedErrorButton);

        fsfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fsfButton.setSelected(!fsfButton.isSelected());
                if (fsfButton.isSelected() && currentPoint != null) {
                    int serv = currentPoint.getScore().getService();
                    if (serv == Constants.PLAYER_A && isPlayerALeft ||
                            serv == Constants.PLAYER_B && !isPlayerALeft) {
                        rightDoubleFaultButton.setEnabled(true);
                    } else {
                        leftDoubleFaultButton.setEnabled(true);
                    }
                } else if (!fsfButton.isSelected()) {
                    leftDoubleFaultButton.setEnabled(false);
                    rightDoubleFaultButton.setEnabled(false);
                }
            }
        });

        TextView categorieTV = ((TextView) findViewById(R.id.categorie));
        if (match.getCategory().equals("SM")) {
            int num = match.getId() % 7;
            categorieTV.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        } else if (match.getCategory().equals("SW")) {
            int num = (match.getId() - 2) % 7;
            categorieTV.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        } else {
            categorieTV.setText(Utils.getCategorieName(match.getCategory()));
        }
        findViewById(R.id.optionsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
                String[] options = {"Abandon", "Inverser le côté des joueurs", "Inverser les serveurs"};
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        optionsSelected(i);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                builder.setCancelable(true);
                builder.show();
            }
        });

        int court = getIntent().getIntExtra(COURT_NUM, -1);
        ((TextView) findViewById(R.id.court)).setText(Utils.getCourtName(court));
        boolean resumeMatch = getIntent().getBooleanExtra(RESUME_MATCH, false);
        if (resumeMatch) {
            automate = FileManager.importAutomate(this);
        } else {
            int service = getIntent().getIntExtra(SERVICE, 0);
            boolean sideLeftA = getIntent().getBooleanExtra(SIDE_LEFT_A, true);
            automate = getAutomateInstance(service, sideLeftA);
        }
        syncManager = new ScoreSyncManager(this, automate);
        Point initPoint = automate.last();
        if (initPoint.getScore().isSideLeftA())
            setPlayerALeft();
        else
            setPlayerARight();
        updateScore(initPoint, true);
        fadeIn(null);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUI(automate.back(), true);
            }
        });
    }

    private void optionsSelected(int i) {
        switch (i) {
            case 0: // Abandon
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.abandon);
                builder.setMessage(R.string.definitive_action);
                builder.setNegativeButton(android.R.string.cancel, null);
                builder.setPositiveButton(R.string.continuer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
                        builder.setTitle(R.string.select_winner);
                        String[] options = {getPlayerName(0), getPlayerName(1)};
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
                                builder.setTitle(R.string.match_ended);
                                final int winner = i;
                                String winnerName = getPlayerName(i);
                                builder.setMessage("Vainqueur: " + winnerName);
                                builder.setNegativeButton(android.R.string.cancel, null);
                                builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final ProgressDialog dialog = new ProgressDialog(MatchActivity.this);
                                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                        dialog.setMessage("Match terminé, Synchronisation en cours...\nVeuillez patienter. ");
                                        dialog.setCancelable(false);
                                        dialog.show();
                                        syncManager.setOnSyncFinishListener(new OnSyncFinishListener() {
                                            @Override
                                            public void onSyncFinish() {
                                                FileManager.deleteFile(MatchActivity.this); // Suppresion de la sauvegarde du match terminé
                                                dialog.cancel();
                                                AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
                                                builder.setTitle("Synchronisation terminée");
                                                builder.setMessage("Retour à l'accueil");
                                                builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Intent intent = new Intent(MatchActivity.this, HomeActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                                builder.show();
                                            }
                                        });
                                        syncManager.abandon(winner);
                                    }
                                });
                                builder.setCancelable(true);
                                builder.show();
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, null);
                        builder.setCancelable(true);
                        builder.show();
                    }
                });
                builder.setCancelable(true);
                builder.show();
                break;
            case 1: {
                Point p = automate.switchSide();
                updateUI(p, false);
                break;
            }
            case 2: {
                Point p = automate.switchService();
                updateUI(p, false);
                break;
            }
            default:
                break;
        }
    }

    protected void updateUI(final Point point, final boolean sync) {
        Score score = point.getScore();
        if (score.isSideLeftA() != isPlayerALeft && score.getVainqueur() == -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Changement de côté")
                    .setCancelable(false)
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            updateScoreAndSide(point, sync);
                        }
                    })
                    .show();
        } else if (score.getVainqueur() != -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Match terminé");
            builder.setMessage("Vainqueur: " + getPlayerName(score.getVainqueur()));
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    updateScore(automate.back(), sync);
                }
            });
            builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    updateScore(point, sync);
                }
            });
            builder.setCancelable(false);
            builder.show();
        } else {
            updateScore(point, sync);
        }
        fsfButton.setSelected(false);
        leftDoubleFaultButton.setEnabled(false);
        rightDoubleFaultButton.setEnabled(false);
    }

    /**
     * Update the score and synchonize it with the server.
     *
     * @param point
     */
    private void updateScore(Point point, boolean sync) {
        currentPoint = point;
        Score score = point.getScore();
        if (score.getPreviousSet() != null) {
            matchScoreView.setScore(score.getPreviousSet().getSetA(), score.getPreviousSet().getSetB(),
                    score.getNumSet() - 1, 0, 0);
        }
        if (score.getVainqueur() != -1) {
            leftButtons.setVisibility(View.GONE);
            rightButtons.setVisibility(View.GONE);
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Match terminé, Synchronisation en cours...\nVeuillez patienter. ");
            dialog.setCancelable(false);
            dialog.show();
            syncManager.setOnSyncFinishListener(new OnSyncFinishListener() {
                @Override
                public void onSyncFinish() {
                    FileManager.deleteFile(MatchActivity.this); // Suppresion de la sauvegarde du match terminé
                    dialog.cancel();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
                    builder.setTitle("Synchronisation terminée");
                    builder.setMessage("Retour à l'accueil");
                    builder.setCancelable(false);
                    builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(MatchActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.show();
                }
            });
        } else {
            leftButtons.setVisibility(View.VISIBLE);
            rightButtons.setVisibility(View.VISIBLE);
            String strScoreA = score.getScoreA() == SimplePoint.AV ? "AV" : String.valueOf(score.getScoreA());
            String strScoreB = score.getScoreB() == SimplePoint.AV ? "AV" : String.valueOf(score.getScoreB());
            scorePlayerA.setText(strScoreA);
            scorePlayerB.setText(strScoreB);
            setService(score.getService());
            matchScoreView.setScore(score.getGamesA(), score.getGamesB(), score.getNumSet(), score.getScoreA(), score.getScoreB());
        }

        if (sync)
            syncManager.addPoint(point);
    }

    /**
     * Makes an update between #fadeOut and #fadeIn
     *
     * @param point
     */
    private void updateScoreAndSide(final Point point, final boolean sync) {
        fadeOut(new Runnable() {
            @Override
            public void run() {
                Score score = point.getScore();
                if (score.isSideLeftA()) {
                    setPlayerALeft();
                } else {
                    setPlayerARight();
                }
                updateScore(point, sync);

                fadeIn(null);
            }
        });

    }

    /**
     * Changes the side of the service-icon indicator
     *
     * @param player the player who serve
     */
    private void setService(int player) {
        if (isPlayerALeft) {
            if (player == 0) {
                serviceLeft.setVisibility(View.VISIBLE);
                serviceRight.setVisibility(View.INVISIBLE);
            } else if (player == 1) {
                serviceLeft.setVisibility(View.INVISIBLE);
                serviceRight.setVisibility(View.VISIBLE);
            }
        } else {
            if (player == 1) {
                serviceLeft.setVisibility(View.VISIBLE);
                serviceRight.setVisibility(View.INVISIBLE);
            } else if (player == 0) {
                serviceLeft.setVisibility(View.INVISIBLE);
                serviceRight.setVisibility(View.VISIBLE);
            }
        }
    }

    protected void setStatsButtonsForPlayerALeft() {
        setStatsButtons(0, 1);
    }

    protected void setStatsButtonsForPlayerARight() {
        setStatsButtons(1, 0);
    }

    private void setStatsButtons(final int leftPlayer, final int rightPlayer) {
        leftPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.FE, fsfButton.isSelected(), leftPlayer)), true);
            }
        });
        leftAceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.ACE, fsfButton.isSelected(), leftPlayer)), true);
            }
        });
        leftDoubleFaultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.DF, true, leftPlayer)), true);
            }
        });
        leftWinnerServeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.WS, fsfButton.isSelected(), leftPlayer)), true);
            }
        });
        leftWinnerPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.WP, fsfButton.isSelected(), leftPlayer)), true);
            }
        });
        leftUnforcedErrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.UE, fsfButton.isSelected(), leftPlayer)), true);
            }
        });

        rightPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.FE, fsfButton.isSelected(), rightPlayer)), true);
            }
        });
        rightAceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.ACE, fsfButton.isSelected(), rightPlayer)), true);
            }
        });
        rightDoubleFaultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.DF, true, rightPlayer)), true);
            }
        });
        rightWinnerServeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.WS, fsfButton.isSelected(), rightPlayer)), true);
            }
        });
        rightWinnerPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.WP, fsfButton.isSelected(), rightPlayer)), true);
            }
        });
        rightUnforcedErrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUI(automate.point(new Stats(Stats.Type.UE, fsfButton.isSelected(), rightPlayer)), true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Do nothing when back button is pressed
    }

    public void onMatchFinishedSyncError() {
        FileManager.deleteFile(MatchActivity.this); // Suppresion de la sauvegarde du match terminé
        AlertDialog.Builder builder = new AlertDialog.Builder(MatchActivity.this);
        builder.setTitle("Match terminé");
        builder.setMessage("Le match a été terminé par un administrateur");
        builder.setCancelable(false);
        builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(MatchActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.show();
    }

    protected abstract String getPlayerName(int player);

    /**
     * Return the automate the use
     *
     * @return
     */
    protected abstract Automate getAutomateInstance(int service, boolean sideLeftA);

    /**
     * Set the UI with PlayerA to the left. Changes reference for scorePlayerA/B and updates the others views (photo, name,etc)
     * Updates #scorePlayerA/B references
     */
    protected abstract void setPlayerALeft();

    /**
     * Set the UI with PlayerA to the right. Changes reference for scorePlayerA/B and updates the others views (photo, name,etc)
     * Updates #scorePlayerA/B references
     */
    protected abstract void setPlayerARight();


    protected void fadeIn(Runnable onComplete) {
        List<View.OnClickListener> leftListeners = new ArrayList<>();
        List<View.OnClickListener> rightListeners = new ArrayList<>();
        Animation appearLeft = AnimationUtils.loadAnimation(this, R.anim.appear_left);
        Animation appearRight = AnimationUtils.loadAnimation(this, R.anim.appear_right);
        AnimationSet appearAnimationSet = new AnimationSet(true);
        appearAnimationSet.addAnimation(appearLeft);
        appearAnimationSet.addAnimation(appearRight);
        leftButtons.setAnimation(appearLeft);
        playerLeft1Photo.setAnimation(appearLeft);
        playerLeft2Photo.setAnimation(appearLeft);
        playerLeft1Name.setAnimation(appearLeft);
        playerLeft2Name.setAnimation(appearLeft);
        playerLeftNat.setAnimation(appearLeft);
        for (SecureDelayButton button : animatedLeftButtons) {
            leftListeners.add(button.getOnClickListener());
            button.setOnClickListener(null);
        }

        rightButtons.setAnimation(appearRight);
        playerRight1Photo.setAnimation(appearRight);
        playerRight2Photo.setAnimation(appearRight);
        playerRight1Name.setAnimation(appearRight);
        playerRight2Name.setAnimation(appearRight);
        playerRightNat.setAnimation(appearRight);
        for (SecureDelayButton button : animatedRightButtons) {
            rightListeners.add(button.getOnClickListener());
            button.setOnClickListener(null);
        }

        appearAnimationSet.start();

        playerLeft1Photo.setVisibility(View.VISIBLE);
        playerLeft2Photo.setVisibility(View.VISIBLE);
        playerLeftNat.setVisibility(View.VISIBLE);
        playerLeft1Name.setVisibility(View.VISIBLE);
        playerLeft2Name.setVisibility(View.VISIBLE);
        int i = 0;
        for (SecureDelayButton button : animatedLeftButtons) {
            button.setOnClickListener(leftListeners.get(i++));
        }
        leftButtons.setVisibility(View.VISIBLE);

        playerRight1Photo.setVisibility(View.VISIBLE);
        playerRight2Photo.setVisibility(View.VISIBLE);
        playerRightNat.setVisibility(View.VISIBLE);
        playerRight1Name.setVisibility(View.VISIBLE);
        playerRight2Name.setVisibility(View.VISIBLE);
        i = 0;
        for (SecureDelayButton button : animatedRightButtons) {
            button.setOnClickListener(rightListeners.get(i++));
        }
        rightButtons.setVisibility(View.VISIBLE);

        if (onComplete != null) {
            new Handler().postDelayed(onComplete, appearAnimationSet.getDuration());
        }
    }

    protected void fadeOut(final Runnable onComplete) {
        for (SecureDelayButton button : animatedLeftButtons) {
            button.setOnClickListener(null);
        }
        for (SecureDelayButton button : animatedRightButtons) {
            button.setOnClickListener(null);
        }
        Animation disappearLeft = AnimationUtils.loadAnimation(this, R.anim.disappear_left);
        Animation disappearRight = AnimationUtils.loadAnimation(this, R.anim.disappear_right);
        final AnimationSet disappearAnimationSet = new AnimationSet(false);
        disappearAnimationSet.addAnimation(disappearLeft);
        disappearAnimationSet.addAnimation(disappearRight);
        disappearAnimationSet.setRepeatCount(0);

        leftButtons.clearAnimation();
        rightButtons.clearAnimation();
        playerLeft1Name.clearAnimation();
        playerLeft1Name.clearAnimation();
        playerRight1Name.clearAnimation();
        playerRight2Name.clearAnimation();

        leftButtons.setAnimation(disappearLeft);
        playerLeft1Photo.setAnimation(disappearLeft);
        playerLeft2Photo.setAnimation(disappearLeft);
        playerLeft1Name.setAnimation(disappearLeft);
        playerLeft2Name.setAnimation(disappearLeft);
        playerLeftNat.setAnimation(disappearLeft);

        rightButtons.setAnimation(disappearRight);
        playerRight1Photo.setAnimation(disappearRight);
        playerRight2Photo.setAnimation(disappearRight);
        playerRight1Name.setAnimation(disappearRight);
        playerRight2Name.setAnimation(disappearRight);
        playerRightNat.setAnimation(disappearRight);
        for (SecureDelayButton button : animatedRightButtons)
            button.setAnimation(disappearRight);

        disappearAnimationSet.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                leftButtons.setVisibility(View.INVISIBLE);
                playerLeft1Photo.setVisibility(View.INVISIBLE);
                playerLeft2Photo.setVisibility(View.INVISIBLE);
                playerLeftNat.setVisibility(View.INVISIBLE);
                playerLeft1Name.setVisibility(View.INVISIBLE);
                playerLeft2Name.setVisibility(View.INVISIBLE);

                rightButtons.setVisibility(View.INVISIBLE);
                playerRight1Photo.setVisibility(View.INVISIBLE);
                playerRight2Photo.setVisibility(View.INVISIBLE);
                playerRightNat.setVisibility(View.INVISIBLE);
                playerRight1Name.setVisibility(View.INVISIBLE);
                playerRight2Name.setVisibility(View.INVISIBLE);

                if (onComplete != null)
                    onComplete.run();
            }
        }, disappearAnimationSet.getDuration());
    }
}
