package fr.istic.masteru.android.automate;

public class Step {
    private InfoScore infoScore;
    private Stats stats;

    public Step(InfoScore infoScore, Stats stats) {
        this.infoScore = infoScore;
        this.stats = stats;
    }

    public InfoScore getInfoScore() {
        return infoScore;
    }

    public void setInfoScore(InfoScore infoScore) {
        this.infoScore = infoScore;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Point getPoint() {
        return new Point(infoScore.getScore(), stats);
    }
}
