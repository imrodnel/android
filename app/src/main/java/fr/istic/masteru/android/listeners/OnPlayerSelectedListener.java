package fr.istic.masteru.android.listeners;

import fr.istic.masteru.android.model.PlayerInfo;

public interface OnPlayerSelectedListener {

    public void onPlayer1Change(PlayerInfo playerInfo);

    public void onPlayer2Change(PlayerInfo playerInfo);
}
