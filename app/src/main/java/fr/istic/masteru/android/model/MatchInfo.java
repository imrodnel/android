package fr.istic.masteru.android.model;

import java.io.Serializable;

public class MatchInfo implements Serializable {

    private int id;
    private TeamInfo teamA, teamB;
    private String category;
    private String tableau;

    public MatchInfo(int id, TeamInfo teamA, TeamInfo teamB, String category, String tableau) {
        this.id = id;
        this.teamA = teamA;
        this.teamB = teamB;
        this.category = category;
        this.tableau = tableau;
    }

    public int getId() {
        return id;
    }

    public TeamInfo getTeamA() {
        return teamA;
    }

    public void setTeamA(TeamInfo teamA) {
        this.teamA = teamA;
    }

    public TeamInfo getTeamB() {
        return teamB;
    }

    public void setTeamB(TeamInfo teamB) {
        this.teamB = teamB;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTableau() {
        return tableau;
    }

    public void setTableau(String tableau) {
        this.tableau = tableau;
    }
}
