package fr.istic.masteru.android.model;

import java.io.Serializable;

public class PlayerInfo implements Serializable {

    public static final int MAN = 1;
    public static final int WOMAN = 2;

    private int id;
    private int gender;
    private String name;
    private String photoUrl;
    private TeamInfo team;

    public PlayerInfo(int id, int gender, String name, String photoUrl, TeamInfo team) {
        this.id = id;
        this.gender = gender;
        this.name = name;
        this.photoUrl = photoUrl;
        this.team = team;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public TeamInfo getTeam() {
        return team;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
