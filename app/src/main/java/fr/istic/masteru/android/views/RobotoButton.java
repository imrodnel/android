package fr.istic.masteru.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import fr.istic.masteru.android.utils.FontUtils;

public class RobotoButton extends Button {

    public RobotoButton(Context context) {
        super(context);
        setTypeface(FontUtils.robotoThinFont);
    }

    public RobotoButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(FontUtils.robotoThinFont);
    }

    public RobotoButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(FontUtils.robotoThinFont);
    }
}
