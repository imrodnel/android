package fr.istic.masteru.android.automate;

/**
 * Created by Spyrit on 15/10/2014.
 */
public class Score {

    private int id;
    private int numSet;
    private int setA;
    private int setB;
    private int gamesA;
    private int gamesB;
    private int scoreA;
    private int scoreB;
    private int service;
    private InfoScore.PreviousSet previousSet;
    private int vainqueur;
    private boolean sideLeftA;

    public Score(int id, int numSet, int setA, int setB, int gamesA, int gamesB, int scoreA, int scoreB, int service, InfoScore.PreviousSet previousSet, int vainqueur, boolean sideLeftA) {
        this.id = id;
        this.numSet = numSet;
        this.setA = setA;
        this.setB = setB;
        this.gamesA = gamesA;
        this.gamesB = gamesB;
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.service = service;
        this.previousSet = previousSet;
        this.vainqueur = vainqueur;
        this.sideLeftA = sideLeftA;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumSet() {
        return numSet;
    }

    public void setNumSet(int numSet) {
        this.numSet = numSet;
    }

    public int getSetA() {
        return setA;
    }

    public void setSetA(int setA) {
        this.setA = setA;
    }

    public int getSetB() {
        return setB;
    }

    public void setSetB(int setB) {
        this.setB = setB;
    }

    public int getGamesA() {
        return gamesA;
    }

    public void setGamesA(int gamesA) {
        this.gamesA = gamesA;
    }

    public int getGamesB() {
        return gamesB;
    }

    public void setGamesB(int gamesB) {
        this.gamesB = gamesB;
    }

    public int getScoreA() {
        return scoreA;
    }

    public void setScoreA(int scoreA) {
        this.scoreA = scoreA;
    }

    public int getScoreB() {
        return scoreB;
    }

    public void setScoreB(int scoreB) {
        this.scoreB = scoreB;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    public InfoScore.PreviousSet getPreviousSet() {
        return previousSet;
    }

    public void setPreviousSet(InfoScore.PreviousSet previousSet) {
        this.previousSet = previousSet;
    }

    public int getVainqueur() {
        return vainqueur;
    }

    public void setVainqueur(int vainqueur) {
        this.vainqueur = vainqueur;
    }

    public boolean isSideLeftA() {
        return sideLeftA;
    }

    public void setSideLeftA(boolean sideLeftA) {
        this.sideLeftA = sideLeftA;
    }

    public void incrId() {
        this.id++;
    }


    @Override
    public String toString() {
        if (previousSet != null) {
            return "automate.InfoScore{" +
                    "id=" + id +
                    "setA=" + setA +
                    ", setB=" + setB +
                    ", gamesA=" + gamesA +
                    ", gamesB=" + gamesB +
                    ", scoreA=" + scoreA +
                    ", scoreB=" + scoreB +
                    ", service=" + service +
                    ", previousSet=" + previousSet.toString() +
                    ", vainqueur=" + vainqueur +
                    ", numSet=" + numSet +
                    ", sideA=" + sideLeftA +
                    '}';
        } else {
            return "automate.InfoScore{" +
                    "id=" + id +
                    "setA=" + setA +
                    ", setB=" + setB +
                    ", gamesA=" + gamesA +
                    ", gamesB=" + gamesB +
                    ", scoreA=" + scoreA +
                    ", scoreB=" + scoreB +
                    ", service=" + service +
                    ", vainqueur=" + vainqueur +
                    ", numSet=" + numSet +
                    ", sideA=" + sideLeftA +
                    '}';
        }
    }

}
