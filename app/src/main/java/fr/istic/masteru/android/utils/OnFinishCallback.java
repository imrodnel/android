package fr.istic.masteru.android.utils;

public interface OnFinishCallback {
    public void onFinish(Object object);
}
