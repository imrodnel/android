package fr.istic.masteru.android.automate;

/**
 * Created by Spyrit on 30/09/2014.
 */
public class InfoScore {

    private int id;
    private int numSet;
    private int setA;
    private int setB;
    private int gamesA;
    private int gamesB;
    private int scoreA;
    private int scoreB;
    private int service;
    private PreviousSet previousSet;
    private int vainqueur;
    private boolean sideLeftA;
    private boolean tieBreak;
    private boolean superTieBreak;
    private int tieService;
    private int firstService;
    private int modSideTie;

    public InfoScore() {
        this.id = 0;
        this.numSet = 0;
        this.setB = 0;
        this.setA = 0;
        this.gamesA = 0;
        this.gamesB = 0;
        this.scoreA = 0;
        this.scoreB = 0;
        this.service = 0;
        this.previousSet = null;
        this.vainqueur = -1;
        this.sideLeftA = false;
        this.firstService = 0;
        this.modSideTie = 0;
    }

    public InfoScore(int id, int numSet, int setA, int setB, int gamesA, int gamesB, int scoreA, int scoreB, int service, PreviousSet previousSet, int vainqueur, boolean sideLeftA, boolean tieBreak, boolean superTieBreak, int tieService, int firstService, int modSideTie) {
        this.id = id;
        this.numSet = numSet;
        this.setB = setB;
        this.setA = setA;
        this.gamesA = gamesA;
        this.gamesB = gamesB;
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.service = service;
        this.previousSet = previousSet;
        this.vainqueur = vainqueur;
        this.sideLeftA = sideLeftA;
        this.tieBreak = tieBreak;
        this.superTieBreak = superTieBreak;
        this.tieService = tieService;
        this.firstService = firstService;
        this.modSideTie = modSideTie;
    }

    public int getVainqueur() {
        return vainqueur;
    }

    public void setVainqueur(int vainqueur) {
        this.vainqueur = vainqueur;
    }

    public boolean isSideLeftA() {
        return sideLeftA;
    }

    public void setSideLeftA(boolean sideLeftA) {
        this.sideLeftA = sideLeftA;
    }

    public void setSideA(boolean sideLeftA) {
        this.sideLeftA = sideLeftA;
    }

    public int getNumSet() {
        return numSet;
    }

    public void setNumSet(int numSet) {
        this.numSet = numSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void incrId() {
        this.id++;
    }

    public PreviousSet getPreviousSet() {
        return previousSet;
    }

    public void setPreviousSet(PreviousSet previousSet) {
        this.previousSet = previousSet;
    }

    public int getSetA() {
        return setA;
    }

    public void setSetA(int setA) {
        this.setA = setA;
    }

    public int getSetB() {
        return setB;
    }

    public void setSetB(int setB) {
        this.setB = setB;
    }

    public int getGamesA() {
        return gamesA;
    }

    public void setGamesA(int gamesA) {
        this.gamesA = gamesA;
    }

    public int getModSideTie() {
        return modSideTie;
    }

    public void setModSideTie(int modSideTie) {
        this.modSideTie = modSideTie;
    }

    public int getFirstService() {
        return firstService;
    }

    public void setFirstService(int firstService) {
        this.firstService = firstService;
    }

    public int getGamesB() {
        return gamesB;
    }

    public void setGamesB(int gamesB) {
        this.gamesB = gamesB;
    }

    @Override
    public String toString() {
        if (previousSet != null) {
            return "automate.InfoScore{" +
                    "id=" + id +
                    "setA=" + setA +
                    ", setB=" + setB +
                    ", gamesA=" + gamesA +
                    ", gamesB=" + gamesB +
                    ", scoreA=" + scoreA +
                    ", scoreB=" + scoreB +
                    ", service=" + service +
                    ", previousSet=" + previousSet.toString() +
                    ", vainqueur=" + vainqueur +
                    ", numSet=" + numSet +
                    ", sideA=" + sideLeftA +
                    '}';
        } else {
            return "automate.InfoScore{" +
                    "id=" + id +
                    "setA=" + setA +
                    ", setB=" + setB +
                    ", gamesA=" + gamesA +
                    ", gamesB=" + gamesB +
                    ", scoreA=" + scoreA +
                    ", scoreB=" + scoreB +
                    ", service=" + service +
                    ", vainqueur=" + vainqueur +
                    ", numSet=" + numSet +
                    ", sideA=" + sideLeftA +
                    '}';
        }
    }

    public int getScoreA() {
        return scoreA;
    }

    public void setScoreA(int scoreA) {
        this.scoreA = scoreA;
    }

    public int getScoreB() {
        return scoreB;
    }

    public void setScoreB(int scoreB) {
        this.scoreB = scoreB;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    public InfoScore copy() {
        return new InfoScore(id, numSet, setA, setB, gamesA, gamesB, scoreA, scoreB, service, previousSet, vainqueur, sideLeftA, tieBreak, superTieBreak, tieService, firstService, modSideTie);
    }

    public Score getScore() {
        return new Score(id, numSet, setA, setB, gamesA, gamesB, scoreA, scoreB, service, previousSet, vainqueur, sideLeftA);
    }

    public boolean isTieBreak() {
        return tieBreak;
    }

    public void setTieBreak(boolean tieBreak) {
        this.tieBreak = tieBreak;
    }

    public boolean isSuperTieBreak() {
        return superTieBreak;
    }

    public void setSuperTieBreak(boolean superTieBreak) {
        this.superTieBreak = superTieBreak;
    }

    public void modSideA() {
        this.setSideA(!this.isSideLeftA());
    }

    public int getTieService() {
        return tieService;
    }

    public void setTieService(int tieService) {
        this.tieService = tieService;
    }

    public static class PreviousSet {
        private int setA;
        private int setB;

        PreviousSet(int setA, int setB) {
            this.setA = setA;
            this.setB = setB;
        }

        public int getSetA() {
            return setA;
        }

        public void setSetA(int setA) {
            this.setA = setA;
        }

        public int getSetB() {
            return setB;
        }

        public void setSetB(int setB) {
            this.setB = setB;
        }

        @Override
        public String toString() {
            return "PreviousSet{" +
                    "setA=" + setA +
                    ", setB=" + setB +
                    '}';
        }
    }
}
