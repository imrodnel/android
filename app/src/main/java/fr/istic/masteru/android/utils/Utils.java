package fr.istic.masteru.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.istic.masteru.android.R;

public class Utils {

    private static Bitmap person;

    /**
     * @param o
     * @param key
     * @return the string value for the key or null
     */
    public static String getStringFromJson(JsonObject o, String key) {
        String s;
        try {
            s = o.get(key).getAsString();
        } catch (Exception e) {
            s = null;
        }
        return s;
    }

    public static int getRefereeingTypeName(int refereeingType) {
        if (refereeingType == Constants.DOUBLE_CLASSIC)
            return R.string.classic_double;
        if (refereeingType == Constants.DOUBLE_PRO)
            return R.string.pro_double;
        return R.string.empty;
    }

    public static String getTableauName(String code) {
        if (code.equals("1/4-P"))
            return "1/4 Main";
        if (code.equals("1/2-P"))
            return "1/2 Main";
        if (code.equals("Finale-P"))
            return "Final Main";
        if (code.equals("1/2-5"))
            return "1/2 5th place";
        if (code.equals("Finale-3"))
            return "Final 3rd place";
        if (code.equals("Finale-5"))
            return "Final 5th place";
        if (code.equals("Finale-7"))
            return "Final 7th place";
        return null;
    }

    public static String getCategorieName(String code) {
        if (code.equals("SM"))
            return "Men's Singles";
        if (code.equals("SW"))
            return "Women's Singles";
        if (code.equals("DM"))
            return "Men's Doubles";
        if (code.equals("DW"))
            return "Women's Doubles";
        if (code.equals("DX"))
            return "Mixed Doubles";
        return null;
    }

    public static String getCourtName(int id) {
        if (id == 1) {
            return "Court central";
        } else {
            return "Court " + (id - 1);
        }
    }

    public static Bitmap getPersonBitmap(Context context) {
        if (person == null) {
            person = BitmapFactory.decodeResource(context.getResources(), R.drawable.person);
        }
        return person;
    }

    public static String getCurrentDateInMySQLFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static void getToken(final Context context, final OnFinishCallback callback) {
        JsonObject params = new JsonObject();
        params.addProperty("login", "perceval");
        params.addProperty("password", "faux");
        Ion.with(context)
                .load("POST", API_URLS.LOGIN)
                .setTimeout(5000)
                .setJsonObjectBody(params)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null && result.has("code")) {
                            int code = result.getAsJsonPrimitive("code").getAsInt();
                            switch (code) {
                                case 0:
                                    Log.d(Constants.TAG, "getToken: TOKEN OK");
                                    String token = result.getAsJsonPrimitive("token").getAsString();
                                    PrefsUtils.setToken(context, token);
                                    callback.onFinish(null);
                                    break;
                                default:
                                    Log.d(Constants.TAG, "getToken: " + code);
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            getToken(context, callback);
                                        }
                                    }, 1000);
                                    break;
                            }
                        } else {
                            if (e != null)
                                Log.d(Constants.TAG, "getToken", e);
                            else
                                Log.d(Constants.TAG, "getToken: error");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getToken(context, callback);
                                }
                            }, 1000);
                        }
                    }
                });
    }

}
