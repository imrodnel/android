package fr.istic.masteru.android.views;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.automate.SimplePoint;

/**
 * Displays the score of a match with one column for each Set and another column for the current game
 */
public class MatchScoreView extends LinearLayout {

    private TextView playerAName, playerBName;
    private int lineMargin;
    private GameScoreColumn gameScoreColumn;
    private List<SetScoreColumn> setScoreColumns = new ArrayList<SetScoreColumn>();

    public MatchScoreView(Context context) {
        super(context);
        init();
    }

    public MatchScoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MatchScoreView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        lineMargin = getResources().getDimensionPixelSize(R.dimen.matchscore_line_margin);

        // First column with players names.
        LinearLayout playerNames = new LinearLayout(getContext());
        playerNames.setOrientation(VERTICAL);
        int paddingRight = getResources().getDimensionPixelSize(R.dimen.matchscore_player_names_padding_right);
        playerNames.setPadding(0, 0, paddingRight, 0);
        playerAName = new TextView(getContext());
        playerAName.setText("Player A");
        playerBName = new TextView(getContext());
        playerBName.setText("Player B");
        playerAName.setLines(1);
        playerBName.setLines(1);
        playerAName.setEllipsize(TextUtils.TruncateAt.END);
        playerBName.setEllipsize(TextUtils.TruncateAt.END);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, lineMargin);
        playerAName.setLayoutParams(params);
        playerBName.setLayoutParams(params);
        playerAName.setTextColor(Color.WHITE);
        playerBName.setTextColor(Color.WHITE);
        playerNames.addView(playerAName);
        playerNames.addView(playerBName);
        addView(playerNames);

        SetScoreColumn column = new SetScoreColumn(getContext());
        setScoreColumns.add(column);
        addView(column);
        gameScoreColumn = new GameScoreColumn(getContext());
        addView(gameScoreColumn, getChildCount());
    }

    /**
     * Update the score of the current game
     *
     * @param scoreA PlayerA score in the current game
     * @param scoreB PlayerB score in the current game
     */
    public void setScore(int scoreA, int scoreB) {
        gameScoreColumn.setScore(scoreA, scoreB);
    }

    /**
     * Update the scores of the current set and game
     *
     * @param gameA  Number of games won in this set
     * @param gameB  Number of games won in this set
     * @param setId  Id of this set
     * @param scoreA PlayerA score in the current game
     * @param scoreB PlayerB score in the current game
     */
    public void setScore(int gameA, int gameB, int setId, int scoreA, int scoreB) {
        while (setScoreColumns.size() <= setId) {
            SetScoreColumn newSetScoreColumn = new SetScoreColumn(getContext());
            setScoreColumns.add(newSetScoreColumn);
            addView(newSetScoreColumn, getChildCount() - 1);
        }
        while (setScoreColumns.size() > setId + 1) {
            removeView(setScoreColumns.get(setScoreColumns.size() - 1));
            setScoreColumns.remove(setScoreColumns.size() - 1);
        }
        setScoreColumns.get(setId).setScore(gameA, gameB);
        setScore(scoreA, scoreB);
    }

    /**
     * Set the name of the two players
     *
     * @param nameA
     * @param nameB
     */
    public void setPlayersNames(String nameA, String nameB) {
        playerAName.setText(nameA);
        playerBName.setText(nameB);
    }

    private class SetScoreColumn extends LinearLayout {

        private TextView scoreA, scoreB;
        private int equalityBackColor, winnerBackColor, loserBackColor;

        public SetScoreColumn(Context context) {
            super(context);
            init();
        }

        public SetScoreColumn(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public SetScoreColumn(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        private void init() {
            setOrientation(VERTICAL);
            setPadding(0, 0, lineMargin, 0);
            scoreA = new TextView(getContext());
            scoreB = new TextView(getContext());
            int paddingLeftRight = getResources().getDimensionPixelSize(R.dimen.matchscore_score_padding_rightleft);
            scoreA.setPadding(paddingLeftRight, 0, paddingLeftRight, 0);
            scoreB.setPadding(paddingLeftRight, 0, paddingLeftRight, 0);
            LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, lineMargin);
            scoreA.setLayoutParams(params);
            equalityBackColor = getResources().getColor(R.color.matchscore_equality);
            winnerBackColor = getResources().getColor(R.color.matchscore_winner);
            loserBackColor = getResources().getColor(R.color.matchscore_loser);
            addView(scoreA);
            addView(scoreB);
            setScore(0, 0);
        }

        public void setScore(int a, int b) {
            scoreA.setText(String.valueOf(a));
            scoreB.setText(String.valueOf(b));
            if (a == b) {
                scoreA.setBackgroundColor(equalityBackColor);
                scoreB.setBackgroundColor(equalityBackColor);
            } else if (a > b) {
                scoreA.setBackgroundColor(winnerBackColor);
                scoreB.setBackgroundColor(loserBackColor);
            } else if (a < b) {
                scoreA.setBackgroundColor(loserBackColor);
                scoreB.setBackgroundColor(winnerBackColor);
            }
        }
    }

    private class GameScoreColumn extends LinearLayout {

        private TextView scoreA, scoreB;

        public GameScoreColumn(Context context) {
            super(context);
            init();
        }

        public GameScoreColumn(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public GameScoreColumn(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        private void init() {
            setOrientation(VERTICAL);
            scoreA = new TextView(getContext());
            scoreB = new TextView(getContext());
            int paddingLeftRight = getResources().getDimensionPixelSize(R.dimen.matchscore_score_padding_rightleft);
            scoreA.setPadding(paddingLeftRight, 0, paddingLeftRight, 0);
            scoreB.setPadding(paddingLeftRight, 0, paddingLeftRight, 0);
            LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, lineMargin);
            scoreA.setLayoutParams(params);
            int background = getResources().getColor(R.color.matchscore_gamescore_background);
            scoreA.setBackgroundColor(background);
            scoreB.setBackgroundColor(background);
            scoreA.setTextColor(Color.WHITE);
            scoreB.setTextColor(Color.WHITE);
            scoreA.setLines(1);
            scoreB.setLines(1);
            addView(scoreA);
            addView(scoreB);
            setScore(0, 0);
        }

        public void setScore(int a, int b) {
            String strSetA = a == SimplePoint.AV ? "ADV" : String.valueOf(a);
            String strSetB = b == SimplePoint.AV ? "ADV" : String.valueOf(b);
            scoreA.setText(strSetA);
            scoreB.setText(strSetB);
        }
    }
}
