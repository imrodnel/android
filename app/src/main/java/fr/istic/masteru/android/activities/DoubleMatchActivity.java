package fr.istic.masteru.android.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.automate.Automate;
import fr.istic.masteru.android.automate.AutomateDouble;
import fr.istic.masteru.android.automate.AutomateDoublePro;
import fr.istic.masteru.android.utils.Utils;

/**
 * Activity pour un match double.
 * Type du match (Double ou DoubleProSet) préciser dans l'intent DoubleMatchActivity#REFEREEING_TYPE)
 */
public class DoubleMatchActivity extends MatchActivity {

    public final static String REFEREEING_TYPE = "REFEREEING_TYPE";
    public final static int TYPE_DOUBLE = 1;
    public final static int TYPE_DOUBLE_PRO = 2;

    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (!getIntent().getBooleanExtra(MatchActivity.RESUME_MATCH, false))
            type = getIntent().getIntExtra(REFEREEING_TYPE, 0);

        super.onCreate(savedInstanceState);

        nameA = playerInfoA1.getName() + "/" + playerInfoA2.getName();
        nameB = playerInfoB1.getName() + "/" + playerInfoB2.getName();
        matchScoreView.setPlayersNames(nameA, nameB);
    }

    @Override
    protected String getPlayerName(int player) {
        if (player == 0) {
            return nameA;
        } else if (player == 1) {
            return nameB;
        } else
            return null;
    }

    @Override
    protected Automate getAutomateInstance(int service, boolean sideLeftA) {
        if (type == TYPE_DOUBLE)
            return new AutomateDouble(this.getApplicationContext(), service, sideLeftA, match.getId());
        else if (type == TYPE_DOUBLE_PRO)
            return new AutomateDoublePro(this.getApplicationContext(), service, sideLeftA, match.getId());
        else
            throw new IllegalArgumentException("DoubleMatchActivity#type must be defined.");
    }

    @Override
    protected void setPlayerALeft() {
        isPlayerALeft = true;
        scorePlayerA = (TextView) findViewById(R.id.scoreLeft);
        scorePlayerB = (TextView) findViewById(R.id.scoreRight);
        playerLeft1Name.setText(playerInfoA1.getName());
        playerLeft2Name.setText(playerInfoA2.getName());
        playerRight1Name.setText(playerInfoB1.getName());
        playerRight2Name.setText(playerInfoB2.getName());
        playerLeftNat.setText(playerInfoA1.getTeam().getName());
        playerRightNat.setText(playerInfoB1.getTeam().getName());
        Ion.with(DoubleMatchActivity.this).load(playerInfoA1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerLeft1Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoA2.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerLeft2Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoB1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerRight1Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoB2.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerRight2Photo.setImageBitmap(result);
            }
        });
        setStatsButtonsForPlayerALeft();
    }

    @Override
    protected void setPlayerARight() {
        isPlayerALeft = false;
        scorePlayerA = (TextView) findViewById(R.id.scoreRight);
        scorePlayerB = (TextView) findViewById(R.id.scoreLeft);
        playerLeft1Name.setText(playerInfoB1.getName());
        playerLeft2Name.setText(playerInfoB2.getName());
        playerRight1Name.setText(playerInfoA1.getName());
        playerRight2Name.setText(playerInfoA2.getName());
        playerLeftNat.setText(playerInfoB1.getTeam().getName());
        playerRightNat.setText(playerInfoA1.getTeam().getName());
        Ion.with(DoubleMatchActivity.this).load(playerInfoA1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerRight1Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoA2.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerRight2Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoB1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerLeft1Photo.setImageBitmap(result);
            }
        });
        Ion.with(DoubleMatchActivity.this).load(playerInfoB2.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(DoubleMatchActivity.this);
                }
                playerLeft2Photo.setImageBitmap(result);
            }
        });
        setStatsButtonsForPlayerARight();
    }

}
