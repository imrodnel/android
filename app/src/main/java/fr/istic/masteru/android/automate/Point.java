package fr.istic.masteru.android.automate;

public class Point {

    private Score score;
    private Stats stats;

    public Point(Score score, Stats stats) {
        this.score = score;
        this.stats = stats;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getBackupString() {
        return stats.getBackupString();
    }
}
