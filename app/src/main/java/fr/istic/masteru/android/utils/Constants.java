package fr.istic.masteru.android.utils;

public class Constants {

    public static final String TAG = "AceCreamTag";

    public static final int SIMPLE = 0;
    public static final int DOUBLE_CLASSIC = 1;
    public static final int DOUBLE_PRO = 2;

    public static final int UNDEFINED = 0;
    public static final int A_IS_LEFT = 1;
    public static final int A_IS_RIGHT = 2;

    public static final int PLAYER_A = 0;
    public static final int PLAYER_B = 1;
}
