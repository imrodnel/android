package fr.istic.masteru.android.utils;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtils {

    public static Typeface robotoThinFont;
    public static Typeface robotoRegularFont;
    public static Typeface robotoBoldFont;

    public static void loadFont(Context context) {
        robotoThinFont = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        robotoBoldFont = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
    }
}
