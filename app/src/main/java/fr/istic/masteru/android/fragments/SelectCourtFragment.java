package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.utils.API_URLS;
import fr.istic.masteru.android.utils.Constants;

public class SelectCourtFragment extends SetupFragment {

    MatchSetupActivity matchSetupActivity;
    private List<View> buttons = new ArrayList<>();
    private View nextButton;
    private View progressBar;
    private View refreshButton;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.select_court_fragment, container, false);
        root.findViewById(R.id.previousButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                matchSetupActivity.previous();
            }
        });
        nextButton = root.findViewById(R.id.nextButton);
        progressBar = root.findViewById(R.id.progressBar);
        refreshButton = root.findViewById(R.id.refreshButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchSetupActivity.next();
            }
        });
        buttons.add(root.findViewById(R.id.court1));
        buttons.add(root.findViewById(R.id.court2));
        buttons.add(root.findViewById(R.id.court3));
        buttons.add(root.findViewById(R.id.court4));
        buttons.add(root.findViewById(R.id.court5));
        buttons.add(root.findViewById(R.id.court6));
        buttons.add(root.findViewById(R.id.court7));
        for (int i = 0; i < buttons.size(); i++) {
            final View button = buttons.get(i);
            final int idCourt = i + 1;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (View button : buttons) {
                        if (button.isEnabled())
                            button.setSelected(false);
                    }
                    button.setSelected(true);
                    matchSetupActivity.onSelectedCourtChange(idCourt);
                    nextButton.setVisibility(View.VISIBLE);
                }
            });
        }
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAvailableCourts();
            }
        });
        loadAvailableCourts();
        return root;
    }

    private void loadAvailableCourts() {
        setRefreshing(true);
        Ion.with(matchSetupActivity)
                .load("GET", API_URLS.AVAILABLE_COURTS)
                .setTimeout(1000 * 6)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        setRefreshing(false);
                        if (e != null || result != null && result.get("code").getAsInt() != 0) {
                            loadAvailableCourts();
                            return;
                        }
                        nextButton.setVisibility(View.INVISIBLE);
                        matchSetupActivity.onSelectedCourtChange(Constants.UNDEFINED);
                        for (View button : buttons) {
                            button.setEnabled(false);
                            button.setSelected(false);
                        }
//                        progressBar.setVisibility(View.GONE);
                        JsonArray courtsArray = result.getAsJsonArray("courts");
                        for (int i = 0; i < courtsArray.size(); i++) {
                            int courtId = courtsArray.get(i).getAsInt();
                            buttons.get(courtId - 1).setEnabled(true);
                        }
                    }
                });
    }

    private void setRefreshing(boolean refreshing) {
        if (refreshing) {
            progressBar.setVisibility(View.VISIBLE);
            refreshButton.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            refreshButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void cancel() {
        matchSetupActivity.onSelectedCourtChange(Constants.UNDEFINED);
    }
}
