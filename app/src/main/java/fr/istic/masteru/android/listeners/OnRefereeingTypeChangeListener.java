package fr.istic.masteru.android.listeners;

public interface OnRefereeingTypeChangeListener {
    public void onRefereeingTypeChange(int type);
}
