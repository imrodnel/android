package fr.istic.masteru.android.listeners;

import fr.istic.masteru.android.model.TeamInfo;

public interface OnFirstServiceChangeListener {
    public void onFirstServiceChange(TeamInfo teamInfo);
}
