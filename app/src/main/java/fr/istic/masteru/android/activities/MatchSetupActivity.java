package fr.istic.masteru.android.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.fragments.ResultFragment;
import fr.istic.masteru.android.fragments.SelectCourtFragment;
import fr.istic.masteru.android.fragments.SelectFirstServiceFragment;
import fr.istic.masteru.android.fragments.SelectInitialSideFragment;
import fr.istic.masteru.android.fragments.SelectMatchFragment;
import fr.istic.masteru.android.fragments.SelectPlayersFragment;
import fr.istic.masteru.android.fragments.SelectRefereeingTypeFragment;
import fr.istic.masteru.android.fragments.SetupFragment;
import fr.istic.masteru.android.listeners.OnFirstServiceChangeListener;
import fr.istic.masteru.android.listeners.OnInitialSideChangeListener;
import fr.istic.masteru.android.listeners.OnMatchSelectedListener;
import fr.istic.masteru.android.listeners.OnRefereeingTypeChangeListener;
import fr.istic.masteru.android.listeners.OnSelectedCourtChangeListener;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.model.TeamInfo;
import fr.istic.masteru.android.utils.API_URLS;
import fr.istic.masteru.android.utils.Constants;
import fr.istic.masteru.android.utils.PrefsUtils;
import fr.istic.masteru.android.utils.Utils;

public class MatchSetupActivity extends Activity implements OnMatchSelectedListener,
        OnRefereeingTypeChangeListener, OnFirstServiceChangeListener, OnInitialSideChangeListener,
        OnSelectedCourtChangeListener {

    private List<String> flow = new ArrayList<String>();
    private int currentPosition = 0;
    private ResultFragment resultFragment;
    private SetupFragment currentSetupFragment;

    private MatchInfo match;
    private int refereeingType;
    private PlayerInfo playerA1, playerA2, playerB1, playerB2;
    private TeamInfo firstServerTeam;
    private int side = Constants.UNDEFINED;
    private int idCourt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.match_setup_activity);
        flow.add(SelectMatchFragment.class.getName());
        flow.add(SelectCourtFragment.class.getName());
        flow.add(SelectPlayersFragment.class.getName());
        flow.add(SelectFirstServiceFragment.class.getName());
        flow.add(SelectInitialSideFragment.class.getName());
        String fname = flow.get(currentPosition);
        if (fname != null) {
            currentSetupFragment = (SetupFragment) Fragment.instantiate(this, fname);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentContainer, currentSetupFragment);
            transaction.commit();
        }
        resultFragment = (ResultFragment) getFragmentManager().findFragmentById(R.id.resultFragment);
    }

    public void next() {
        currentPosition++;
        if (currentPosition == flow.size()) {
            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            startMatch(dialog);
            return;
        }
        String fname = flow.get(currentPosition);
        if (fname != null) {
            currentSetupFragment = (SetupFragment) Fragment.instantiate(this, fname);
            if (currentSetupFragment instanceof SelectPlayersFragment) {
                ((SelectPlayersFragment) currentSetupFragment).setMatch(match);
            }
            if (currentSetupFragment instanceof SelectFirstServiceFragment) {
                ((SelectFirstServiceFragment) currentSetupFragment).setMatch(match);
            }
            if (currentSetupFragment instanceof SelectInitialSideFragment) {
                ((SelectInitialSideFragment) currentSetupFragment).setMatch(match);
            }
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.animator.translation_in_left, R.animator.translation_out_left);
            transaction.replace(R.id.fragmentContainer, currentSetupFragment);
            transaction.commit();
        }
    }

    public void previous() {
        currentPosition--;
        String fname = flow.get(currentPosition);
        if (fname != null) {
            currentSetupFragment = (SetupFragment) Fragment.instantiate(this, fname);
            if (currentSetupFragment instanceof SelectPlayersFragment) {
                ((SelectPlayersFragment) currentSetupFragment).setMatch(match);
            }
            if (currentSetupFragment instanceof SelectFirstServiceFragment) {
                ((SelectFirstServiceFragment) currentSetupFragment).setMatch(match);
            }
            if (currentSetupFragment instanceof SelectInitialSideFragment) {
                ((SelectInitialSideFragment) currentSetupFragment).setMatch(match);
            }
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.animator.translation_in_right, R.animator.translation_out_right);
            transaction.replace(R.id.fragmentContainer, currentSetupFragment);
            transaction.commit();
        }
    }

    private void startMatch(final ProgressDialog dialogToClose) {
        final JsonObject param = new JsonObject();
        param.addProperty("action", "START");
        param.addProperty("court", idCourt);
        param.addProperty("startDate", Utils.getCurrentDateInMySQLFormat());
        param.addProperty("service", firstServerTeam.equals(match.getTeamA()) ? 0 : 1);
        param.addProperty("playerAEq1Id", playerA1.getId());
        param.addProperty("playerAEq2Id", playerB1.getId());
        if (refereeingType == Constants.SIMPLE) {
            param.addProperty("playerBEq1Id", 0);
            param.addProperty("playerBEq2Id", 0);
        } else {
            param.addProperty("playerBEq1Id", playerA2.getId());
            param.addProperty("playerBEq2Id", playerB2.getId());
        }

        Ion.with(this)
                .load("PUT", API_URLS.UPDATE_MATCH(match.getId()))
                .setTimeout(1000 * 6)
                .setHeader(API_URLS.AUTH_TOKEN, PrefsUtils.getToken(this))
                .setJsonObjectBody(param)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null || result != null && result.get("code").getAsInt() != 0) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startMatch(dialogToClose);
                                }
                            }, 1000);
                        } else {
                            dialogToClose.cancel();
                            if (refereeingType == Constants.SIMPLE) {
                                Intent matchActivityIntent = new Intent(MatchSetupActivity.this, SingleMatchActivity.class);
                                matchActivityIntent.putExtra(MatchActivity.MATCH, match);
                                matchActivityIntent.putExtra(MatchActivity.COURT_NUM, idCourt);
                                matchActivityIntent.putExtra(MatchActivity.SIDE_LEFT_A, side == Constants.A_IS_LEFT);
                                matchActivityIntent.putExtra(MatchActivity.SERVICE, firstServerTeam.getISO().equals(match.getTeamA().getISO()) ? 0 : 1);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_A1, playerA1);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_B1, playerB1);
                                startActivity(matchActivityIntent);
                            } else {
                                Intent matchActivityIntent = new Intent(MatchSetupActivity.this, DoubleMatchActivity.class);
                                matchActivityIntent.putExtra(MatchActivity.MATCH, match);
                                matchActivityIntent.putExtra(MatchActivity.COURT_NUM, idCourt);
                                matchActivityIntent.putExtra(MatchActivity.SIDE_LEFT_A, side == Constants.A_IS_LEFT);
                                matchActivityIntent.putExtra(MatchActivity.SERVICE, firstServerTeam.getISO().equals(match.getTeamA().getISO()) ? 0 : 1);
                                matchActivityIntent.putExtra(DoubleMatchActivity.REFEREEING_TYPE, refereeingType);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_A1, playerA1);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_A2, playerA2);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_B1, playerB1);
                                matchActivityIntent.putExtra(MatchActivity.PLAYER_B2, playerB2);
                                startActivity(matchActivityIntent);
                            }
                            finish();
                        }
                    }
                });
    }

    @Override
    public void onMatchSelected(MatchInfo match) {
        this.match = match;
        if (resultFragment != null)
            resultFragment.setMatchInfos(match);
        if (match != null && match.getCategory().contains("D")
                && !flow.get(1).equals(SelectRefereeingTypeFragment.class.getName())) {
            flow.add(1, SelectRefereeingTypeFragment.class.getName());
        } else if (match != null && !match.getCategory().contains("D") &&
                flow.get(1).equals(SelectRefereeingTypeFragment.class.getName())) {
            flow.remove(1);
        }
    }

    @Override
    public void onRefereeingTypeChange(int type) {
        refereeingType = type;
        if (resultFragment != null)
            resultFragment.setRefereeingType(type);
    }

    @Override
    public void onBackPressed() {
        if (currentPosition > 0) {
            currentSetupFragment.cancel();
            previous();
        } else {
            super.onBackPressed();
        }
    }

    public void onPlayerA1Change(PlayerInfo playerInfo) {
        if (resultFragment != null)
            resultFragment.setPlayerA1(playerInfo);
        playerA1 = playerInfo;
    }

    public void onPlayerA2Change(PlayerInfo playerInfo) {
        if (resultFragment != null)
            resultFragment.setPlayerA2(playerInfo);
        playerA2 = playerInfo;
    }

    public void onPlayerB1Change(PlayerInfo playerInfo) {
        if (resultFragment != null)
            resultFragment.setPlayerB1(playerInfo);
        playerB1 = playerInfo;
    }

    public void onPlayerB2Change(PlayerInfo playerInfo) {
        if (resultFragment != null)
            resultFragment.setPlayerB2(playerInfo);
        playerB2 = playerInfo;
    }

    @Override
    public void onFirstServiceChange(TeamInfo teamInfo) {
        if (resultFragment != null)
            resultFragment.setFirstService(teamInfo);
        firstServerTeam = teamInfo;
    }

    @Override
    public void onInitialSideChange(int side) {
        if (resultFragment != null)
            resultFragment.setServiceSide(side);
        this.side = side;
    }

    @Override
    public void onSelectedCourtChange(int idCourt) {
        if (resultFragment != null)
            resultFragment.setCourt(idCourt);
        this.idCourt = idCourt;
    }
}
