package fr.istic.masteru.android.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.automate.Automate;
import fr.istic.masteru.android.automate.AutomateSimple;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.utils.Utils;

public class SingleMatchActivity extends MatchActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        playerInfoA1 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_A1);
        playerInfoB1 = (PlayerInfo) getIntent().getSerializableExtra(PLAYER_B1);

        super.onCreate(savedInstanceState);

        hideUnusedViews();

        if (match.getCategory().equals("SH")) {
            int num = match.getId() % 7;
            ((TextView) findViewById(R.id.categorie)).setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        } else if (match.getCategory().equals("SF")) {
            int num = (match.getId() - 2) % 7;
            ((TextView) findViewById(R.id.categorie)).setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        }
        matchScoreView.setPlayersNames(playerInfoA1.getName(), playerInfoB1.getName());
    }

    @Override
    protected String getPlayerName(int player) {
        if (player == 0) {
            return playerInfoA1.getName();
        } else if (player == 1) {
            return playerInfoB1.getName();
        } else
            return null;
    }

    @Override
    protected Automate getAutomateInstance(int service, boolean sideLeftA) {
        return new AutomateSimple(this.getApplicationContext(), service, sideLeftA, match.getId());
    }

    @Override
    protected void setPlayerALeft() {
        isPlayerALeft = true;
        scorePlayerA = (TextView) findViewById(R.id.scoreLeft);
        scorePlayerB = (TextView) findViewById(R.id.scoreRight);
        playerLeft1Name.setText(playerInfoA1.getName());
        playerRight1Name.setText(playerInfoB1.getName());
        playerLeftNat.setText(playerInfoA1.getTeam().getName());
        playerRightNat.setText(playerInfoB1.getTeam().getName());
        Ion.with(SingleMatchActivity.this).load(playerInfoA1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(SingleMatchActivity.this);
                }
                playerLeft1Photo.setImageBitmap(result);
            }
        });
        Ion.with(SingleMatchActivity.this).load(playerInfoB1.getPhotoUrl()).asBitmap().setCallback(new FutureCallback<Bitmap>() {
            @Override
            public void onCompleted(Exception e, Bitmap result) {
                if (result == null) {
                    result = Utils.getPersonBitmap(SingleMatchActivity.this);
                }
                playerRight1Photo.setImageBitmap(result);
            }
        });
        setStatsButtonsForPlayerALeft();
    }

    @Override
    protected void setPlayerARight() {
        isPlayerALeft = false;
        scorePlayerA = (TextView) findViewById(R.id.scoreRight);
        scorePlayerB = (TextView) findViewById(R.id.scoreLeft);
        playerLeft1Name.setText(playerInfoB1.getName());
        playerRight1Name.setText(playerInfoA1.getName());
        playerLeftNat.setText(playerInfoB1.getTeam().getName());
        playerRightNat.setText(playerInfoA1.getTeam().getName());
        Ion.with(SingleMatchActivity.this).load(playerInfoA1.getPhotoUrl())
                .asBitmap()
                .setCallback(new FutureCallback<Bitmap>() {
                    @Override
                    public void onCompleted(Exception e, Bitmap result) {
                        if (result == null) {
                            result = Utils.getPersonBitmap(SingleMatchActivity.this);
                        }
                        playerRight1Photo.setImageBitmap(result);
                    }
                });
        Ion.with(SingleMatchActivity.this).load(playerInfoB1.getPhotoUrl())
                .asBitmap()
                .setCallback(new FutureCallback<Bitmap>() {
                    @Override
                    public void onCompleted(Exception e, Bitmap result) {
                        if (result == null) {
                            result = Utils.getPersonBitmap(SingleMatchActivity.this);
                        }
                        playerLeft1Photo.setImageBitmap(result);
                    }
                });
        setStatsButtonsForPlayerARight();
    }

    @Override
    protected void fadeIn(Runnable onComplete) {
        super.fadeIn(onComplete);
        hideUnusedViews();
    }

    /**
     * Removes views which are not used in Single mode
     */
    private void hideUnusedViews() {
        playerLeft2Name.setVisibility(View.GONE);
        playerLeft2Photo.setVisibility(View.GONE);
        playerRight2Name.setVisibility(View.GONE);
        playerRight2Photo.setVisibility(View.GONE);
    }
}
