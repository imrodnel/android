package fr.istic.masteru.android.automate;

public class Stats {

    public static enum Type {
        NONE,
        ACE,
        WS, // Winner Serve
        WP, // Winner point
        DF, // Double Fault
        UE, // Unforcer Error
        FE, // Forced Error
    }

    private Type type;
    private boolean fsf;
    private boolean breakPoint;
    private int winner;

    public Stats() {
        this.type = Type.NONE;
        this.fsf = false;
        this.winner = -1;
    }

    public Stats(Type type, boolean fsf, int winner) {
        this.type = type;
        this.fsf = fsf;
        this.winner = winner;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isFsf() {
        return fsf;
    }

    public void setFsf(boolean fsf) {
        this.fsf = fsf;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public boolean isBreakPoint() {
        return breakPoint;
    }

    public void setBreakPoint(boolean breakPoint) {
        this.breakPoint = breakPoint;
    }

    public Stats copy() {
        return new Stats(this.type, this.fsf, this.winner);
    }

    public String getBackupString() {
        StringBuilder builder = new StringBuilder();
        builder.append("!");
        builder.append(type.name());
        builder.append(":");
        builder.append(fsf);
        builder.append(":");
        builder.append(Integer.toString(winner));
        return builder.toString();
    }

    public static Stats createFromBackup(String save) {
        String[] tab = save.split(":");
        Type type = Type.valueOf(tab[0]);
        boolean fsf = Boolean.valueOf(tab[1]);
        int winner = Integer.parseInt(tab[2]);
        return new Stats(type, fsf, winner);
    }
}
