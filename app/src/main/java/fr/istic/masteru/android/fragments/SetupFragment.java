package fr.istic.masteru.android.fragments;

import android.app.Fragment;

public abstract class SetupFragment extends Fragment {
    public abstract void cancel();
}
