package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.adapters.MatchAdapter;
import fr.istic.masteru.android.listeners.OnMatchSelectedListener;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.TeamInfo;
import fr.istic.masteru.android.utils.API_URLS;

public class SelectMatchFragment extends SetupFragment implements OnMatchSelectedListener {

    MatchSetupActivity matchSetupActivity;
    private List<MatchInfo> matches = new ArrayList<MatchInfo>();
    private MatchAdapter matchAdapter;
    private Button nextButton;
    private View progressBar;
    private View refreshButton;
    private TextView messageTV;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.select_match_fragment, container, false);
        nextButton = (Button) root.findViewById(R.id.nextButton);
        progressBar = root.findViewById(R.id.progressBar);
        refreshButton = root.findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMatches();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchSetupActivity.next();
            }
        });
        messageTV = (TextView) root.findViewById(R.id.messageTV);
        ListView matchListView = (ListView) root.findViewById(R.id.matchListView);
        matchListView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.divider_height));
        matchAdapter = new MatchAdapter(matchSetupActivity, matches, this);
        matchListView.setAdapter(matchAdapter);

        loadMatches();

        return root;
    }

    private void loadMatches() {
        setRefreshing(true);
        Ion.with(matchSetupActivity)
                .load("GET", API_URLS.MATCHS + "?round=CURRENT")
                .setTimeout(1000 * 10)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        setRefreshing(false);
                        if (e != null || result != null && result.get("code").getAsInt() != 0) {
                            if (result.get("code").getAsInt() == -3)
                                displayMessage(matchSetupActivity.getString(R.string.no_match_setting_up));
                            else
                                loadMatches();
                            return;
                        }
                        matches.clear();
                        matchAdapter.resetSelection();
                        JsonArray matchesArray = result.getAsJsonArray("matches");
                        for (int i = 0; i < matchesArray.size(); i++) {
                            JsonObject o = matchesArray.get(i).getAsJsonObject();
                            String status = o.get("Statut").getAsString();
                            if (!status.equals("SOON")) // Display only the matches which can be played
                                continue;
                            int idMatch = o.get("IdMatch").getAsInt();
                            String isoA = o.get("Team_A").getAsString();
                            String isoB = o.get("Team_B").getAsString();
                            String teamNameA = o.get("NameTeam_A").getAsString();
                            String teamNameB = o.get("NameTeam_B").getAsString();
                            String flagA = o.get("UrlTeam_A").getAsString();
                            String flagB = o.get("UrlTeam_B").getAsString();
                            TeamInfo teamA = new TeamInfo(isoA, teamNameA, flagA);
                            TeamInfo teamB = new TeamInfo(isoB, teamNameB, flagB);
                            String category = o.get("Category").getAsString();
                            String tableau = o.get("Tableau").getAsString();
                            matches.add(new MatchInfo(idMatch, teamA, teamB, category, tableau));
                        }
                        if (matches.size() == 0) {
                            displayMessage(matchSetupActivity.getString(R.string.no_matchs_all_ended));
                        }
                        if (matchAdapter != null)
                            matchAdapter.notifyDataSetChanged();
                    }
                });
    }

    private void displayMessage(String message) {
        progressBar.setVisibility(View.GONE);
        messageTV.setText(message);
    }

    private void setRefreshing(boolean refreshing) {
        if (refreshing) {
            progressBar.setVisibility(View.VISIBLE);
            refreshButton.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            refreshButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMatchSelected(MatchInfo match) {
        if (match == null) {
            nextButton.setVisibility(View.INVISIBLE);
        } else {
            nextButton.setVisibility(View.VISIBLE);
        }
        matchSetupActivity.onMatchSelected(match);
    }

    @Override
    public void cancel() {
        matchSetupActivity.onMatchSelected(null);
    }
}
