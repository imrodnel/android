package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.model.MatchInfo;

public class SelectFirstServiceFragment extends SetupFragment {

    MatchSetupActivity matchSetupActivity;
    private MatchInfo match;
    private int lightGrey;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        lightGrey = matchSetupActivity.getResources().getColor(R.color.light_grey);
        View root = inflater.inflate(R.layout.select_first_service_fragment, container, false);
        root.findViewById(R.id.previousButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                matchSetupActivity.previous();
            }
        });
        final View nextButton = root.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchSetupActivity.next();
            }
        });
        final Button countryAButton = (Button) root.findViewById(R.id.countryA);
        final Button countryBButton = (Button) root.findViewById(R.id.countryB);
        countryAButton.setText(match.getTeamA().getName());
        countryBButton.setText(match.getTeamB().getName());
        countryAButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryAButton.setSelected(true);
                countryBButton.setSelected(false);
                matchSetupActivity.onFirstServiceChange(match.getTeamA());
                nextButton.setVisibility(View.VISIBLE);
            }
        });
        countryBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryAButton.setSelected(false);
                countryBButton.setSelected(true);
                matchSetupActivity.onFirstServiceChange(match.getTeamB());
                nextButton.setVisibility(View.VISIBLE);
            }
        });
        return root;
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
    }

    @Override
    public void cancel() {
        matchSetupActivity.onFirstServiceChange(null);
    }
}
