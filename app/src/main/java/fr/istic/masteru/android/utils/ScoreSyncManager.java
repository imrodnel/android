package fr.istic.masteru.android.utils;

import android.os.Handler;
import android.util.Log;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.Map;
import java.util.TreeMap;

import fr.istic.masteru.android.activities.MatchActivity;
import fr.istic.masteru.android.automate.Automate;
import fr.istic.masteru.android.automate.Point;
import fr.istic.masteru.android.automate.Score;
import fr.istic.masteru.android.listeners.OnSyncFinishListener;

/**
 * Used to manage the synchronisation of the scores with the API.
 * Supports timout errors, mixed scoreId.
 */
public class ScoreSyncManager {

    /**
     * Timeout in ms.
     */
    private static final int TIMEOUT_MS = 10 * 1000;
    /**
     * Delay before retry after a timeout.
     */
    private static final int RETRY_DELAY = 3 * 1000;
    private static final String TAG = "SCORE_SYNC_MANAGER";

    private MatchActivity matchActivity;
    private Automate automate;
    private int idMatch;
    private TreeMap<Float, Object> syncQueue = new TreeMap<>();
    private boolean isSyncing = false;
    private int scoreIdMax;

    /**
     * Listener called when the synchronization is finished
     */
    private OnSyncFinishListener syncFinishListener;

    public ScoreSyncManager(MatchActivity matchActivity, Automate automate) {
        this.matchActivity = matchActivity;
        this.idMatch = automate.getIdMatch();
        this.automate = automate;
    }

    /**
     * Add a point to synchronize.
     * Starts the synchronizations if needed.
     *
     * @param point The point to synchronize
     */
    public void addPoint(Point point) {
        addPoint(point, true);
    }

    /**
     * Add a point to synchronize
     * If canBeBack is set to false, the score will not be considered as a step backward (but as a
     * another try to synchronize an old score).
     *
     * @param point
     * @param canBeBack true if the score can be a step backward, false otherwise.
     */
    private void addPoint(Point point, boolean canBeBack) {
        Score score = point.getScore();
        if (score.getId() < scoreIdMax && canBeBack) {
            back(score.getId());
        } else if (score.getId() > 0) {
            if (score.getVainqueur() == -1) {
                syncQueue.put((float) score.getId(), point);
                if (canBeBack)
                    scoreIdMax = score.getId();
            } else {
                WinnerToSync winnerToSync = new WinnerToSync(score.getVainqueur() == 0 ? "A" : "B", false);
                syncQueue.put(score.getId() + 0.2f, winnerToSync);
            }
            if (score.getPreviousSet() != null) {
                PreviousSetToSync setToSync = new PreviousSetToSync(score.getNumSet(),
                        score.getPreviousSet().getSetA(),
                        score.getPreviousSet().getSetB());
                syncQueue.put(score.getId() + 0.1f, setToSync);
            }
        }
        if (!isSyncing()) {
            syncNextElement();
        }
    }

    public void abandon(int winner) {
        WinnerToSync winnerToSync = new WinnerToSync(winner == 0 ? "A" : "B", true);
        syncQueue.put(scoreIdMax + 0.9f, winnerToSync);
        if (!isSyncing()) {
            syncNextElement();
        }
    }

    /**
     * Sync back to the id (inclusive)
     *
     * @param id
     */
    private void back(int id) {
        syncQueue.tailMap(id + 1.0f, true).clear();
        scoreIdMax = id;
        BackToSync backToSync = new BackToSync(id + 1);
        syncQueue.put(id + 0.3f, backToSync);
    }

    /**
     * Gets the next score to synchronize (with the lowest Id) and calls #synchronizePoint(fr.istic.masteru.android.automate.Score)
     * or do nothing if #scores is empty.
     */
    private void syncNextElement() {
        Map.Entry<Float, Object> firstEntry = syncQueue.firstEntry();
        if (firstEntry == null) {
            setSyncing(false);
            return;
        }
        if (firstEntry.getValue() instanceof Point) {
            setSyncing(true);
            synchronizePoint(firstEntry.getKey(), (Point) firstEntry.getValue());
        } else if (firstEntry.getValue() instanceof PreviousSetToSync) {
            setSyncing(true);
            synchronizePreviousSet(firstEntry.getKey(), (PreviousSetToSync) firstEntry.getValue());
        } else if (firstEntry.getValue() instanceof WinnerToSync) {
            setSyncing(true);
            syncWinner(firstEntry.getKey(), (WinnerToSync) firstEntry.getValue());
        } else if (firstEntry.getValue() instanceof BackToSync) {
            setSyncing(true);
            syncBack(firstEntry.getKey(), (BackToSync) firstEntry.getValue());
        }
    }

    /**
     * Calls the API to synchronise the score.
     * If the synchronization is OK, calls syncNextElement automatically.
     * In case of timeout(#TIMEOUT_MS), calls syncNextElement after #RETRY_DELAY ms.
     *
     * @param point
     */
    private void synchronizePoint(final float key, final Point point) {
        final Score score = point.getScore();
        JsonObject param = new JsonObject();
        param.addProperty("scoreA", score.getScoreA());
        param.addProperty("scoreB", score.getScoreB());
        param.addProperty("setNum", score.getNumSet() + 1);
        param.addProperty("setA", score.getSetA());
        param.addProperty("setB", score.getSetB());
        param.addProperty("gameA", score.getGamesA());
        param.addProperty("gameB", score.getGamesB());
        param.addProperty("server", score.getService());
        param.addProperty("pointWinner", point.getStats().getWinner() == 0 ? "A" : "B");
        param.addProperty("stats", point.getStats().getType().name());
        param.addProperty("fsf", point.getStats().isFsf() ? "1" : "0");
        param.addProperty("break", point.getStats().isBreakPoint() ? "1" : "0");

        Ion.with(matchActivity)
                .load("POST", API_URLS.UPDATE_SCORE(idMatch, score.getId()))
                .setTimeout(TIMEOUT_MS)
                .setHeader(API_URLS.AUTH_TOKEN, PrefsUtils.getToken(matchActivity))
                .setJsonObjectBody(param)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null && result.has("code")) {
                            int code = result.getAsJsonPrimitive("code").getAsInt();
                            switch (code) {
                                case 0:
                                    // Sync OK
                                    Log.d(TAG, "SyncScore " + code);
                                    syncQueue.remove(key);
                                    break;
                                case -1:
                                    Log.d(TAG, "SyncScore " + code);
                                    break;
                                case -2:
                                    Log.d(TAG, "SyncScore " + code);
                                    break;
                                case -3:
                                    Log.d(TAG, "SyncScore " + code);
                                    int oldPointId = result.getAsJsonPrimitive("oldPointId").getAsInt();
                                    for (int i = oldPointId + 1; i < score.getId(); i++) {
                                        addPoint(automate.getPoint(i));
                                    }
                                    break;
                                case -4: // Duplicate entry
                                    Log.d(TAG, "SyncScore " + code);
                                    syncQueue.remove(key);
                                    break;
                                case -5: // Match not in progress
                                    matchActivity.onMatchFinishedSyncError();
                                    return;
                            }
                            syncNextElement();
                        } else {
                            if (e != null)
                                Log.d(TAG, "SyncScore ", e);
                            else
                                Log.d(TAG, "SyncScore error");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    syncNextElement();
                                }
                            }, RETRY_DELAY);
                        }
                    }
                });
    }

    /**
     * * Calls the API to synchronize the PreviousSet value.
     * If the synchronization is OK, calls #syncNextPreviousSet automatically.
     * In case of timeout(#TIMEOUT_MS), calls #syncNextPreviousSet after #RETRY_DELAY ms.
     *
     * @param key         key value in #syncQueue
     * @param previousSet Previous set to sync.
     */
    private void synchronizePreviousSet(final float key, PreviousSetToSync previousSet) {
        JsonObject param = new JsonObject();
        param.addProperty("gameA", previousSet.getSetA());
        param.addProperty("gameB", previousSet.getSetB());

        Ion.with(matchActivity)
                .load("PUT", API_URLS.UPDATE_SET_SCORE(idMatch, previousSet.getNumSet()))
                .setTimeout(TIMEOUT_MS)
                .setHeader(API_URLS.AUTH_TOKEN, PrefsUtils.getToken(matchActivity))
                .setJsonObjectBody(param)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null && result.has("code")) {
                            int code = result.getAsJsonPrimitive("code").getAsInt();
                            switch (code) {
                                case 0:
                                    // Sync OK
                                    Log.d(TAG, "SyncPreviousSet " + code);
                                    syncQueue.remove(key);
                                    break;
                                case -2:
                                    Log.d(TAG, "SyncPreviousSet " + code);
                                    break;
                            }
                            syncNextElement();
                        } else {
                            if (e != null)
                                Log.d(TAG, "SyncPreviousSet ", e);
                            else
                                Log.d(TAG, "SyncPreviousSet error");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    syncNextElement();
                                }
                            }, RETRY_DELAY);
                        }
                    }
                });
    }


    /**
     * @param key
     * @param backToSync
     */
    private void syncBack(final float key, BackToSync backToSync) {
        Ion.with(matchActivity)
                .load("DELETE", API_URLS.DELETE_SCORE(idMatch, backToSync.getId()))
                .setTimeout(TIMEOUT_MS)
                .setHeader(API_URLS.AUTH_TOKEN, PrefsUtils.getToken(matchActivity))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null && result.has("code")) {
                            int code = result.getAsJsonPrimitive("code").getAsInt();
                            switch (code) {
                                case 0:
                                    // Sync OK
                                    Log.d(TAG, "Back " + code);
                                    syncQueue.remove(key);
                                    break;
                                case -2:
                                    Log.d(TAG, "Back " + code);
                                    break;
                                case -3:
                                    Log.d(TAG, "Back " + code);
                                    syncQueue.remove(key);
                                    break;
                            }
                            syncNextElement();
                        } else {
                            if (e != null)
                                Log.d(TAG, "Back ", e);
                            else
                                Log.d(TAG, "Back error");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    syncNextElement();
                                }
                            }, RETRY_DELAY);
                        }
                    }
                });
    }

    /**
     * @param key
     * @param winnerToSync
     */

    private void syncWinner(final float key, WinnerToSync winnerToSync) {
        JsonObject param = new JsonObject();
        param.addProperty("action", winnerToSync.isAbandon() ? "ABANDON" : "END");
        param.addProperty("winner", winnerToSync.getWinner());
        param.addProperty("endDate", Utils.getCurrentDateInMySQLFormat());

        Ion.with(matchActivity)
                .load("PUT", API_URLS.UPDATE_MATCH(idMatch))
                .setTimeout(TIMEOUT_MS)
                .setHeader(API_URLS.AUTH_TOKEN, PrefsUtils.getToken(matchActivity))
                .setJsonObjectBody(param)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null && result.has("code")) {
                            int code = result.getAsJsonPrimitive("code").getAsInt();
                            switch (code) {
                                case 0:
                                    // Sync OK
                                    Log.d(TAG, "SyncWinner " + code);
                                    syncQueue.remove(key);
                                    if (syncFinishListener != null)
                                        syncFinishListener.onSyncFinish();
                                    break;
                            }
                            syncNextElement();
                        } else {
                            if (e != null)
                                Log.d(TAG, "SyncWinner ", e);
                            else
                                Log.d(TAG, "SyncWinner error");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    syncNextElement();
                                }
                            }, RETRY_DELAY);
                        }
                    }
                });
    }

    public void setOnSyncFinishListener(OnSyncFinishListener syncFinishListener) {
        this.syncFinishListener = syncFinishListener;
    }

    public boolean isSyncing() {
        return isSyncing;
    }

    public void setSyncing(boolean isSyncing) {
        this.isSyncing = isSyncing;
    }

    static class PreviousSetToSync {

        private int numSet;
        private int setA, setB;

        PreviousSetToSync(int numSet, int setA, int setB) {
            this.numSet = numSet;
            this.setA = setA;
            this.setB = setB;
        }

        public int getNumSet() {
            return numSet;
        }

        public int getSetA() {
            return setA;
        }

        public int getSetB() {
            return setB;
        }
    }

    static class WinnerToSync {

        private String winner;
        private boolean abandon;

        WinnerToSync(String winner, boolean abandon) {
            this.winner = winner;
            this.abandon = abandon;
        }

        public String getWinner() {
            return winner;
        }

        public boolean isAbandon() {
            return abandon;
        }
    }

    static class BackToSync {

        private int id;

        BackToSync(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}
