package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.adapters.PlayerAdapter;
import fr.istic.masteru.android.listeners.OnPlayerSelectedListener;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.model.PlayerInfo;
import fr.istic.masteru.android.model.TeamInfo;
import fr.istic.masteru.android.utils.API_URLS;
import fr.istic.masteru.android.utils.Utils;

public class SelectPlayersFragment extends SetupFragment {

    private static final int TEAM_A = 0;
    private static final int TEAM_B = 1;

    MatchSetupActivity matchSetupActivity;
    private List<PlayerInfo> playersA = new ArrayList<PlayerInfo>();
    private List<PlayerInfo> playersB = new ArrayList<PlayerInfo>();
    private PlayerAdapter playerAdapterA, playerAdapterB;
    private Button nextButton;
    private ProgressBar progressBar;
    private PlayerInfo playerA1, playerA2, playerB1, playerB2;
    private MatchInfo match;
    private boolean isDouble;
    private boolean isMixed;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.select_players_fragment, container, false);
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        nextButton = (Button) root.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchSetupActivity.next();
            }
        });
        root.findViewById(R.id.previousButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                matchSetupActivity.previous();
            }
        });

        ((TextView) root.findViewById(R.id.countryA)).setText(match.getTeamA().getName());
        ((TextView) root.findViewById(R.id.countryB)).setText(match.getTeamB().getName());

        ListView playerAList = (ListView) root.findViewById(R.id.playerAListView);
        ListView playerBList = (ListView) root.findViewById(R.id.playerBListView);
        playerAList.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.divider_height));
        playerBList.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.divider_height));
        playerAdapterA = new PlayerAdapter(matchSetupActivity, playersA, isDouble, new OnPlayerSelectedListener() {
            @Override
            public void onPlayer1Change(PlayerInfo playerInfo) {
                playerA1 = playerInfo;
                checkSelectionComplete();
                matchSetupActivity.onPlayerA1Change(playerInfo);
            }

            @Override
            public void onPlayer2Change(PlayerInfo playerInfo) {
                playerA2 = playerInfo;
                checkSelectionComplete();
                matchSetupActivity.onPlayerA2Change(playerInfo);
            }
        });
        playerAdapterB = new PlayerAdapter(matchSetupActivity, playersB, isDouble, new OnPlayerSelectedListener() {
            @Override
            public void onPlayer1Change(PlayerInfo playerInfo) {
                playerB1 = playerInfo;
                checkSelectionComplete();
                matchSetupActivity.onPlayerB1Change(playerInfo);
            }

            @Override
            public void onPlayer2Change(PlayerInfo playerInfo) {
                playerB2 = playerInfo;
                checkSelectionComplete();
                matchSetupActivity.onPlayerB2Change(playerInfo);
            }
        });
        playerAList.setAdapter(playerAdapterA);
        playerBList.setAdapter(playerAdapterB);

        loadPlayers(match.getTeamA(), playersA, playerAdapterA);
        loadPlayers(match.getTeamB(), playersB, playerAdapterB);

        return root;
    }

    private void checkSelectionComplete() {
        if ((!isDouble && playerA1 != null && playerB1 != null)
                || (isDouble && !isMixed && playerA1 != null && playerA2 != null && playerB1 != null && playerB2 != null)
                || (isDouble && isMixed && playerA1 != null && playerA2 != null && playerB1 != null && playerB2 != null
                && playerA1.getGender() != playerA2.getGender() && playerB1.getGender() != playerB2.getGender())) {
            nextButton.setVisibility(View.VISIBLE);
        } else {
            nextButton.setVisibility(View.INVISIBLE);
        }
    }

    private void loadPlayers(final TeamInfo team, final List<PlayerInfo> players, final PlayerAdapter adapter) {
        String gender = match.getCategory().contains("M") ? "M" : match.getCategory().contains("W") ? "W" : "X";

        String url = API_URLS.TEAM_PLAYERS(team.getISO(), gender);
        Log.d("URL", url);
        Ion.with(matchSetupActivity)
                .load("GET", url)
                .setTimeout(1000 * 10)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null || result != null && result.get("code").getAsInt() != 0) {
                            loadPlayers(team, players, adapter);
                            return;
                        }
                        progressBar.setVisibility(View.GONE);
                        JsonArray playerArray = result.getAsJsonArray("players");
                        for (int i = 0; i < playerArray.size(); i++) {
                            JsonObject o = playerArray.get(i).getAsJsonObject();
                            int playerId = o.get("IdPlayer").getAsInt();
                            String name = o.get("Name").getAsString();
                            String firstName = o.get("FirstName").getAsString();
                            String photoUrl = Utils.getStringFromJson(o, "Url");
                            int gender = o.get("Sex").getAsString().equals("M") ? PlayerInfo.MAN : PlayerInfo.WOMAN;
                            PlayerInfo playerInfo = new PlayerInfo(playerId, gender, firstName + " " + name, photoUrl, team);
                            players.add(playerInfo);
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    public void setMatch(MatchInfo match) {
        this.match = match;
        isDouble = match.getCategory().contains("D");
        isMixed = match.getCategory().equals("DX");
    }

    @Override
    public void cancel() {
        matchSetupActivity.onPlayerA1Change(null);
        matchSetupActivity.onPlayerA2Change(null);
        matchSetupActivity.onPlayerB1Change(null);
        matchSetupActivity.onPlayerB2Change(null);
    }
}
