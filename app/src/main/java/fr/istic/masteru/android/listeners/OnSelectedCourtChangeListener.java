package fr.istic.masteru.android.listeners;

public interface OnSelectedCourtChangeListener {
    public void onSelectedCourtChange(int idCourt);
}
