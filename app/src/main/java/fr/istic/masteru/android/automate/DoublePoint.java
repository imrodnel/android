package fr.istic.masteru.android.automate;

/**
 * Created by Spyrit on 01/10/2014.
 */
public class DoublePoint {

    public static final int[][] action = {
    /* état          1  2 */
    /*0:0*/         {1, 2},
    /*15:0*/        {1, 2},
    /*0:15*/        {1, 2},
    /*15:15*/       {1, 2},
    /*30:0*/        {3, 2},
    /*0:30*/        {1, 4},
    /*30:15*/       {3, 2},
    /*15:30*/       {1, 4},
    /*30:30*/       {3, 4},
    /*40:0*/        {5, 2},
    /*0:40*/        {1, 6},
    /*40:15*/       {5, 2},
    /*15:40*/       {1, 6},
    /*40:30*/       {5, 4},
    /*30:40*/       {3, 6},
    /*40:40*/       {5, 6}
    };
    public static final int[][] transit = {
    /* état          1  2 */
    /*0:0*/         {1, 2},
    /*15:0*/        {4, 3},
    /*0:15*/        {3, 5},
    /*15:15*/       {6, 7},
    /*30:0*/        {9, 6},
    /*0:30*/        {7, 10},
    /*30:15*/       {11, 8},
    /*15:30*/       {8, 12},
    /*30:30*/       {13, 14},
    /*40:0*/        {0, 11},
    /*0:40*/        {12, 0},
    /*40:15*/       {0, 13},
    /*15:40*/       {14, 0},
    /*40:30*/       {0, 15},
    /*30:40*/       {15, 0},
    /*40:40*/       {0, 0},
    };
    public static InfoScore score;
    private static int a, b, c, d, e, f;

    public static InfoScore executer(int numact) {
        switch (numact) {
             /* Init service B avec A à gauche*/
            case -11:
                /* Init score, tie-break & tie-service */
                score = new InfoScore(0, 0, 0, 0, 0, 0, 0, 0, 1, null, -1, true, false, false, 1, 0, 6);
                break;
            /* Init service A avec A à gauche*/
            case -10:
                /* Init score, tie-break & tie-service */
                score = new InfoScore(0, 0, 0, 0, 0, 0, 0, 0, 0, null, -1, true, false, false, 1, 0, 6);
                break;
            /* Init service B avec A à droite*/
            case -1:
                /* Init score, tie-break & tie-service */
                score = new InfoScore(0, 0, 0, 0, 0, 0, 0, 0, 1, null, -1, false, false, false, 1, 0, 6);
                break;
            /* Init service A avec A à droite*/
            case 0:
                /* Init score, tie-break & tie-service */
                score = new InfoScore(0, 0, 0, 0, 0, 0, 0, 0, 0, null, -1, false, false, false, 1, 0, 6);
                break;
            /* A marque 15 point */
            case 1:
                score.setPreviousSet(null);
                score.setScoreA(score.getScoreA() + 15);
                score.incrId();
                break;
            /* B marque 15 point */
            case 2:
                score.setPreviousSet(null);
                score.setScoreB(score.getScoreB() + 15);
                score.incrId();
                break;
            /* A marque 10 point */
            case 3:
                score.setScoreA(score.getScoreA() + 10);
                score.incrId();
                break;
            /* B marque 10 point */
            case 4:
                score.setScoreB(score.getScoreB() + 10);
                score.incrId();
                break;
            /* A marque un jeu */
            case 5:
                /* Mise à jour InfoScore A & B */
                score.setScoreA(0);
                score.setScoreB(0);

                /* Récupération des Jeux */
                a = score.getGamesA();
                b = score.getGamesB();

                /* Récupération des Sets */
                c = score.getSetA();
                d = score.getSetB();

                /* Incrémentation Games de A */
                a++;

                /* Vérification si A marque un set */
                if ((a >= 6) && ((a - b) >= 2)) {

                    /* Incrémentation Set de A */
                    c++;

                    /* Vérifie fin du match */
                    if (c == 2) {
                        score.setVainqueur(0);
                    }

                    /* Mise à jour numéro Set en cours & du score du Set précédent */
                    score.setNumSet(c + d);
                    score.setPreviousSet(new InfoScore.PreviousSet(a, b));

                    /* Algorithme de changement de côté pour un jeu marqué */
                    if (((a + b) % 2) != 0) {
                    /* Le côté change */
                        score.modSideA();
                    }

                    /* Mise à jour Games A & B */
                    a = 0;
                    b = 0;


                    /* Vérification SuperTieBreak */
                    if ((c == 1) && (d == 1)) {
                        score.setSuperTieBreak(true);
                    }

                /* Vérification si Cas de tieBreak */
                } else if ((a == 6) && (b == 6)) {
                    score.setTieBreak(true);
                }
                /* Mise à jour Games A & B ainsi que Set A */
                score.setGamesA(a);
                score.setGamesB(b);
                score.setSetA(c);

                /* Mise à jour service */
                score.setService((score.getService() + 1) % 2);

                /* Cas Tie-Break, enregistrement premier serveur */
                score.setFirstService(score.getService());

                 /* Algorithme de changement de côté pour un jeu marqué */
                if (((score.getGamesA() + score.getGamesB()) % 2) != 0) {
                    /* Le côté change */
                    score.modSideA();
                }


                score.incrId();
                break;
            /* B marque un jeu */
            case 6:
                /* Mise à jour InfoScore A & B */
                score.setScoreA(0);
                score.setScoreB(0);

                /* Récupération des Jeux */
                a = score.getGamesA();
                b = score.getGamesB();

                /* Récupération des Sets */
                c = score.getSetA();
                d = score.getSetB();

                /* Incrémentation Games de B */
                b++;

                /* Vérification si B marque un set */
                if ((b >= 6) && ((b - a) >= 2)) {

                    /* Incrémentation Set de B */
                    d++;

                    /* Vérification fin du match */
                    if (d == 2) {
                        score.setVainqueur(1);
                    }

                    /* Mise à jour numéro Set en cours & du score du Set précédent */
                    score.setNumSet(c + d);
                    score.setPreviousSet(new InfoScore.PreviousSet(a, b));

                    /* Algorithme de changement de côté pour un jeu marqué */
                    if (((a + b) % 2) != 0) {
                    /* Le côté change */
                        score.modSideA();
                    }

                    /* Mise à jour Games A & B */
                    a = 0;
                    b = 0;


                    /* Vérification SuperTieBreak */
                    if ((c == 1) && (d == 1)) {
                        score.setSuperTieBreak(true);
                    }

                /* Vérification si Cas de tieBreak */
                } else if ((a == 6) && (b == 6)) {
                    score.setTieBreak(true);
                }

                /* Mise à jour Games A & B ainsi que Set B */
                score.setGamesA(a);
                score.setGamesB(b);
                score.setSetB(d);

                /* Mise à jour service */
                score.setService((score.getService() + 1) % 2);

                /* Cas Tie-Break, enregistrement premier serveur */
                score.setFirstService(score.getService());

                 /* Algorithme de changement de côté pour un jeu marqué */
                if (((score.getGamesA() + score.getGamesB()) % 2) != 0) {
                    /* Le côté change */
                    score.modSideA();
                }

                score.incrId();
                break;
            /** A marque au tie-break => Gagne un set */
            case 10:
                /* Mettre set précédent à null*/
                score.setPreviousSet(null);

                /* Récupération des scores */
                a = score.getScoreA();
                b = score.getScoreB();
                c = score.getGamesA();
                d = score.getGamesB();
                e = score.getSetA();
                f = score.getSetB();

                /* Ajouter le point */
                a++;

                /* Vérification fin du tie-break */
                if ((a >= 7) && ((a - b) >= 2)) {

                    /* Incrémentation Games A (pour l'enregistrement dans précédent) et Set A*/
                    c++;
                    e++;

                    /* Vérification fin du match */
                    if (e == 2) {
                        score.setVainqueur(0);
                    }

                    /* Mise à jour des scores */
                    score.setScoreA(0);
                    score.setScoreB(0);
                    score.setGamesB(0);
                    score.setGamesA(0);

                    /* Mise à jour du numéro du set en cours */
                    score.setNumSet(e + f);

                    /* Mise à jour du match précédent */
                    score.setPreviousSet(new InfoScore.PreviousSet(c, d));

                    /* Mise à jour Set de A */
                    score.setSetA(e);

                    /* Mise à jour du service */
                    score.setService((score.getFirstService() + 1) % 2);

                     /* Fin de l'état tie-break */
                    score.setTieBreak(false);

                    /* Remise du nombre de service en tie-break à 1 */
                    score.setTieService(1);

                    /* Vérification SuperTieBreak */
                    if (e == 1 && f == 1) score.setSuperTieBreak(true);

                    /* Le côté change */
                    score.modSideA();

                    /* Remise de l'indice à 6 */
                    score.setModSideTie(6);
                } else {
                    /* set InfoScore de A */
                    score.setScoreA(a);

                    /* Calcul pour déterminer le serveur */
                    score.setTieService(score.getTieService() - 1);
                    if (score.getTieService() == 0) {
                        score.setTieService(2);
                        score.setService((score.getService() + 1) % 2);
                    }

                    /* Algorithme de changement de côté pour un tie-break*/
                    score.setModSideTie((score.getModSideTie() - 1));
                    if (score.getModSideTie() == 0) {
                    /* Le côté change */
                        score.modSideA();
                    /* Remise de l'indice à 6 */
                        score.setModSideTie(6);
                    }
                }

                score.incrId();
                break;
            /** B marque au tie-break => Gagne un set*/
            case 11:
                /* Mettre set précédent à null*/
                score.setPreviousSet(null);

                /* Récupération des scores */
                a = score.getScoreA();
                b = score.getScoreB();
                c = score.getGamesA();
                d = score.getGamesB();
                e = score.getSetA();
                f = score.getSetB();

                /* Ajouter le point */
                b++;

                /* Vérification fin du tie-break */
                if ((b >= 7) && ((b - a) >= 2)) {

                    /* Incrémentation Games B (pour l'enregistrement dans précédent) et Set B*/
                    d++;
                    f++;

                    /* Vérification fin du match */
                    if (e == 2) {
                        score.setVainqueur(1);
                    }

                    /* Mise à jour des scores */
                    score.setScoreA(0);
                    score.setScoreB(0);
                    score.setGamesB(0);
                    score.setGamesA(0);

                    /* Mise à jour du numéro du set en cours */
                    score.setNumSet(e + f);

                    /* Mise à jour du match précédent */
                    score.setPreviousSet(new InfoScore.PreviousSet(c, d));

                    /* Mise à jour Set de B */
                    score.setSetB(f);

                    /* Mise à jour du service */
                    score.setService((score.getFirstService() + 1) % 2);

                     /* Fin de l'état tie-break */
                    score.setTieBreak(false);

                    /* Remise du nombre de service en tie-break à 1 */
                    score.setTieService(1);

                    /* Vérification SuperTieBreak */
                    if (e == 1 && f == 1) score.setSuperTieBreak(true);

                    /* Le côté change */
                    score.modSideA();

                    /* Remise de l'indice à 6 */
                    score.setModSideTie(6);
                } else {
                    /* set InfoScore de B */
                    score.setScoreB(b);

                    /* Calcul pour déterminer le serveur */
                    score.setTieService(score.getTieService() - 1);
                    if (score.getTieService() == 0) {
                        score.setTieService(2);
                        score.setService((score.getService() + 1) % 2);
                    }

                    /* Algorithme de changement de côté pour un tie-break*/
                    score.setModSideTie((score.getModSideTie() - 1));
                    if (score.getModSideTie() == 0) {
                    /* Le côté change */
                        score.modSideA();
                    /* Remise de l'indice à 6 */
                        score.setModSideTie(6);
                    }

                }

                score.incrId();
                break;
            /** A marque au super_tie-break */
            case 20:
                /* Mettre set précédent à null*/
                score.setPreviousSet(null);

                /* Récupération des scores */
                a = score.getScoreA();
                b = score.getScoreB();
                c = score.getGamesA();
                d = score.getGamesB();
                e = score.getSetA();
                f = score.getSetB();

                /* Ajouter le point */
                a++;

                /* Vérification fin du super-tie-break */
                if ((a >= 10) && ((a - b) >= 2)) {

                    /* Incrémentation Set de A (pour l'affichage sur nb set) */
                    e++;

                    /* A est vainqueur du match */
                    score.setVainqueur(0);

                    /* Mise à jour des scores */
                    score.setScoreA(0);
                    score.setScoreB(0);
                    score.setGamesB(0);
                    score.setGamesA(0);

                    /* Mise à jour du numéro du set en cours */
                    score.setNumSet(e + f);

                    /* Mise à jour du match précédent */
                    score.setPreviousSet(new InfoScore.PreviousSet(a, b));

                    /* Mise à jour Set de A */
                    score.setSetA(e);

                    /* Mise à jour du service */
                    score.setService(score.getFirstService());

                    /** Fin de l'état superTieBreak */
                    score.setSuperTieBreak(false);

                    /* Remise du nombre de service en tie-break à 1 */
                    score.setTieService(1);

                    /* Le côté change */
                    score.modSideA();

                    /* Remise de l'indice à 6 */
                    score.setModSideTie(6);
                } else {
                    /* Mise à jour InfoScore de A */
                    score.setScoreA(a);

                    /* Calcul pour déterminer le serveur */
                    score.setTieService(score.getTieService() - 1);
                    if (score.getTieService() == 0) {
                        score.setTieService(2);
                        score.setService((score.getService() + 1) % 2);
                    }

                    /* Algorithme de changement de côté pour un tie-break*/
                    score.setModSideTie((score.getModSideTie() - 1));
                    if (score.getModSideTie() == 0) {
                    /* Le côté change */
                        score.modSideA();
                    /* Remise de l'indice à 6 */
                        score.setModSideTie(6);
                    }
                }

                score.incrId();
                break;
            /** B marque au super_tie-break */
            case 21:
                /* Mettre set précédent à null*/
                score.setPreviousSet(null);

                /* Récupération des scores */
                a = score.getScoreA();
                b = score.getScoreB();
                c = score.getGamesA();
                d = score.getGamesB();
                e = score.getSetA();
                f = score.getSetB();

                /* Ajouter le point */
                b++;

                /* Vérification fin du super-tie-break */
                if ((b >= 10) && ((b - a) >= 2)) {

                    /* Incrémentation Set de B (pour l'affichage sur nb set) */
                    f++;

                    /* B est vainqueur du match */
                    score.setVainqueur(1);

                    /* Mise à jour des scores */
                    score.setScoreA(0);
                    score.setScoreB(0);
                    score.setGamesB(0);
                    score.setGamesA(0);

                    /* Mise à jour du numéro du set en cours */
                    score.setNumSet(e + f);

                    /* Mise à jour du match précédent */
                    score.setPreviousSet(new InfoScore.PreviousSet(a, b));

                    /* Mise à jour Set de B */
                    score.setSetB(f);

                    /* Mise à jour du service */
                    score.setService(score.getFirstService());

                    /** Fin de l'état superTieBreak */
                    score.setSuperTieBreak(false);

                    /* Remise du nombre de service en tie-break à 1 */
                    score.setTieService(1);

                    /* Le côté change */
                    score.modSideA();

                    /* Remise de l'indice à 6 */
                    score.setModSideTie(6);
                } else {
                    /* Mise à jour InfoScore de B*/
                    score.setScoreB(b);

                    /* Calcul pour déterminer le serveur */
                    score.setTieService(score.getTieService() - 1);
                    if (score.getTieService() == 0) {
                        score.setTieService(2);
                        score.setService((score.getService() + 1) % 2);
                    }

                    /* Algorithme de changement de côté pour un tie-break*/
                    score.setModSideTie((score.getModSideTie() - 1));
                    if (score.getModSideTie() == 0) {
                    /* Le côté change */
                        score.modSideA();
                    /* Remise de l'indice à 6 */
                        score.setModSideTie(6);
                    }
                }

                score.incrId();
                break;
        }
        return score;
    }

    public static InfoScore getScore() {
        return score;
    }

    public static void setScore(InfoScore newScore) {
        score = newScore.copy();
    }

}
