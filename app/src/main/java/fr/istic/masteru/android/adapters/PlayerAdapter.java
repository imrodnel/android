package fr.istic.masteru.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.listeners.OnPlayerSelectedListener;
import fr.istic.masteru.android.model.PlayerInfo;

public class PlayerAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private Context context;
    private List<PlayerInfo> players;
    private OnPlayerSelectedListener onPlayerSelectedListener;
    private int selectedItem1 = -1;
    private int selectedItem2 = -1;
    private boolean isDouble;

    private int normalTextColor;
    private int selectedTextColor;

    public PlayerAdapter(Context context, List<PlayerInfo> players, boolean isDouble,
                         OnPlayerSelectedListener listener) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.players = players;
        this.isDouble = isDouble;
        this.onPlayerSelectedListener = listener;
        normalTextColor = Color.parseColor("#111111");
        selectedTextColor = Color.parseColor("#ffffff");
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int i) {
        return players.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.player_adapter, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.photo = (ImageView) itemView.findViewById(R.id.photo);
            viewHolder.name = (TextView) itemView.findViewById(R.id.name);
            viewHolder.gender = (TextView) itemView.findViewById(R.id.gender);
            itemView.setTag(viewHolder);
        }
        final PlayerInfo playerInfo = players.get(position);
        final ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        Ion.with(viewHolder.photo).fadeIn(false).load(playerInfo.getPhotoUrl());
        viewHolder.name.setText(playerInfo.getName());
        viewHolder.gender.setText(playerInfo.getGender() == PlayerInfo.MAN ? "(M)" : "(W)");

        final View finalItemView = itemView;
        if (position == selectedItem1 || position == selectedItem2) {
            select(finalItemView);
        } else {
            unselect(finalItemView);
        }

        if (isDouble) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == selectedItem1) {
                        unselect(finalItemView);
                        selectedItem1 = -1;
                        onPlayerSelectedListener.onPlayer1Change(null);
                    } else if (position == selectedItem2) {
                        unselect(finalItemView);
                        selectedItem2 = -1;
                        onPlayerSelectedListener.onPlayer2Change(null);
                    } else {
                        select(finalItemView);
                        if (selectedItem1 == -1) {
                            selectedItem1 = position;
                            onPlayerSelectedListener.onPlayer1Change(playerInfo);
                        } else if (selectedItem2 == -1) {
                            selectedItem2 = position;
                            onPlayerSelectedListener.onPlayer2Change(playerInfo);
                        } else {
                            selectedItem1 = selectedItem2;
                            selectedItem2 = position;
                            onPlayerSelectedListener.onPlayer1Change(players.get(selectedItem1));
                            onPlayerSelectedListener.onPlayer2Change(playerInfo);
                        }
                        notifyDataSetChanged();
                    }
                }
            });
        } else {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == selectedItem1) {
                        unselect(finalItemView);
                        selectedItem1 = -1;
                        onPlayerSelectedListener.onPlayer1Change(null);
                    } else {
                        select(finalItemView);
                        selectedItem1 = position;
                        notifyDataSetChanged();
                        onPlayerSelectedListener.onPlayer1Change(playerInfo);
                    }
                }
            });
        }
        return itemView;
    }

    private void select(View itemView) {
        itemView.setBackgroundResource(R.drawable.ac_button_selected);
        ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        viewHolder.name.setTextColor(selectedTextColor);
        viewHolder.gender.setTextColor(selectedTextColor);
    }

    private void unselect(View itemView) {
        itemView.setBackgroundResource(R.drawable.ac_button_normal);
        ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        viewHolder.name.setTextColor(normalTextColor);
        viewHolder.gender.setTextColor(normalTextColor);
    }

    static class ViewHolder {
        ImageView photo;
        TextView name;
        TextView gender;
    }

}
