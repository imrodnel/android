package fr.istic.masteru.android.automate;

/**
 * Created by Spyrit on 01/10/2014.
 */
public interface Automate {

    public Point point(Stats stats);

    public Point pointWithoutSaving(Stats stats);

    public Point getPoint(int index);

    public Point back();

    public Point last();

    public Point switchSide();

    public Point switchService();

    public int getIdMatch();

}
