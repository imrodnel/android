package fr.istic.masteru.android.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import fr.istic.masteru.android.utils.FontUtils;

public class RobotoTextView extends TextView {

    public RobotoTextView(Context context) {
        super(context);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(FontUtils.robotoBoldFont);
        } else if (style == Typeface.ITALIC) { // Dirty hack here. ITALIC == THIN
            super.setTypeface(FontUtils.robotoThinFont);
        } else {
            super.setTypeface(FontUtils.robotoRegularFont);
        }
    }
}
