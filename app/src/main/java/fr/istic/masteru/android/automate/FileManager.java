package fr.istic.masteru.android.automate;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import fr.istic.masteru.android.R;

public class FileManager {

    /* Création du fichier */
    /* S:0:1:46 */
    public static void createFile(Context context, int typeAutomate, int service, int side, int idMatch) {
        deleteFile(context);
        String r;
        if (idMatch < 10) {
            r = "0" + Integer.toString(idMatch);
        } else {
            r = Integer.toString(idMatch);
        }

        if (typeAutomate == 0) {
            WriteScore(context, "S" + Integer.toString(service) + Integer.toString(side) + r);
        } else if (typeAutomate == 1) {
            WriteScore(context, "D" + Integer.toString(service) + Integer.toString(side) + r);
        } else {
            WriteScore(context, "P" + Integer.toString(service) + Integer.toString(side) + r);
        }

    }

    /* Le joueur A ou B marque un point */
    public static void point(Context context, Point point) {
        WriteScore(context, point.getBackupString());
    }

    /* Initialiser un nouvel automate */
    public static Automate importAutomate(Context context) {
        char automate;
        Automate aut;
        int service, side, idMatch;
        char decimal, unite;
        String s = ReadScore(context);
        automate = s.charAt(0);
        service = Character.getNumericValue(s.charAt(1));
        side = Character.getNumericValue(s.charAt(2));
        decimal = s.charAt(3);
        unite = s.charAt(4);
        idMatch = Integer.valueOf("" + decimal + unite);

        if (automate == 'S') {
            aut = new AutomateSimple(context, service, side == 1, idMatch, false);
        } else if (automate == 'D') {
            aut = new AutomateDouble(context, service, side == 1, idMatch, false);
        } else {
            aut = new AutomateDoublePro(context, service, side == 1, idMatch, false);
        }

        String[] backupPoints = s.split("!");
        for (int i = 1; i < backupPoints.length; i++) {
            Stats stats = Stats.createFromBackup(backupPoints[i]);
            aut.pointWithoutSaving(stats);
        }
        return aut;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static File getSaveFile(Context context) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/acecream");
        myDir.mkdirs();
        File file = new File(myDir, "save_" + context.getString(R.string.app_name));
        return file;
    }


    public static void WriteScore(Context context, String data) {
        FileWriter fileWriter;
        BufferedWriter bufferWritter;
        File file;
        if (isExternalStorageWritable()) {
            file = getSaveFile(context);

            try {
                fileWriter = new FileWriter(file, true);
                bufferWritter = new BufferedWriter(fileWriter);
                bufferWritter.write(data);
                bufferWritter.close();
            } catch (IOException e) {
//                Toast.makeText(context, "Erreur lors de l'écriture", Toast.LENGTH_SHORT).show();
            }

        } else {
//            Toast.makeText(context, "Ecriture impossible", Toast.LENGTH_SHORT).show();
        }

    }

    public static String ReadScore(Context context) {
        FileReader fileReader;
        BufferedReader bufferedReader;
        File file;
        String data = null;
        if (isExternalStorageWritable()) {
            file = getSaveFile(context);

            try {
                fileReader = new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                data = bufferedReader.readLine();
                bufferedReader.close();
            } catch (IOException e) {
//                Toast.makeText(context, "Erreur lors de la lecture", Toast.LENGTH_SHORT).show();
            }

        } else {
//            Toast.makeText(context, "Lecture impossible", Toast.LENGTH_SHORT).show();
        }
        return data;
    }

    /* Détruire le fichier */
    public static void deleteFile(Context context) {
        boolean result;
        File file;
        file = getSaveFile(context);
        file.delete();
    }

    public static void deleteLast(Context context) {
        String s = ReadScore(context);
        int startLastElement = s.lastIndexOf("!");
        if (startLastElement != -1) {
            deleteFile(context);
            WriteScore(context, s.substring(0, startLastElement));
        }
    }

    public static boolean fileExist(Context context) {
        File file;
        file = getSaveFile(context);
        return file.exists();
    }
}
