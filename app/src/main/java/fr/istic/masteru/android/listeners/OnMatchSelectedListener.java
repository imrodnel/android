package fr.istic.masteru.android.listeners;

import fr.istic.masteru.android.model.MatchInfo;

public interface OnMatchSelectedListener {
    public void onMatchSelected(MatchInfo match);
}
