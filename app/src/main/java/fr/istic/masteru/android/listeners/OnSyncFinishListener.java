package fr.istic.masteru.android.listeners;

public interface OnSyncFinishListener {
    public void onSyncFinish();
}
