package fr.istic.masteru.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.listeners.OnMatchSelectedListener;
import fr.istic.masteru.android.model.MatchInfo;
import fr.istic.masteru.android.utils.Utils;

public class MatchAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private Context context;
    private List<MatchInfo> matches;
    private OnMatchSelectedListener onMatchSelectedListener;
    private int selectedItem = -1;

    private int normalTextColor;
    private int selectedTextColor;

    public MatchAdapter(Context context, List<MatchInfo> matches, OnMatchSelectedListener listener) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.matches = matches;
        this.onMatchSelectedListener = listener;
        normalTextColor = Color.parseColor("#111111");
        selectedTextColor = Color.parseColor("#ffffff");
    }

    @Override
    public int getCount() {
        return matches.size();
    }

    @Override
    public Object getItem(int i) {
        return matches.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = inflater.inflate(R.layout.match_adapter, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.drapeauA = (ImageView) itemView.findViewById(R.id.drapeauA);
            viewHolder.drapeauB = (ImageView) itemView.findViewById(R.id.drapeauB);
            viewHolder.paysA = (TextView) itemView.findViewById(R.id.paysA);
            viewHolder.paysB = (TextView) itemView.findViewById(R.id.paysB);
            viewHolder.categorie = (TextView) itemView.findViewById(R.id.categorie);
            viewHolder.tableau = (TextView) itemView.findViewById(R.id.tableau);
            viewHolder._vs = (TextView) itemView.findViewById(R.id._vs);
            viewHolder._separator = (TextView) itemView.findViewById(R.id._separator);
            itemView.setTag(viewHolder);
        }
        final MatchInfo match = matches.get(position);
        ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        Ion.with(viewHolder.drapeauA)
                .fadeIn(false)
                .load(match.getTeamA().getFlagUrl());
        Ion.with(viewHolder.drapeauB)
                .fadeIn(false)
                .load(match.getTeamB().getFlagUrl());
        viewHolder.paysA.setText(match.getTeamA().getName());
        viewHolder.paysB.setText(match.getTeamB().getName());
        if (match.getCategory().equals("SM")) {
            int num = match.getId() % 7;
            viewHolder.categorie.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        } else if (match.getCategory().equals("SW")) {
            int num = (match.getId() - 2) % 7;
            viewHolder.categorie.setText(Utils.getCategorieName(match.getCategory()) + " n°" + num);
        } else {
            viewHolder.categorie.setText(Utils.getCategorieName(match.getCategory()));
        }
        viewHolder.tableau.setText(Utils.getTableauName(match.getTableau()));
        final View finalItemView = itemView;
        if (position == selectedItem) {
            select(finalItemView);
        } else {
            unselect(finalItemView);
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position != selectedItem) {
                    notifyDataSetChanged();
                    select(finalItemView);
                    selectedItem = position;
                    onMatchSelectedListener.onMatchSelected(match);
                } else {
                    unselect(finalItemView);
                    selectedItem = -1;
                    onMatchSelectedListener.onMatchSelected(null);
                }
            }
        });
        return itemView;
    }

    private void select(View itemView) {
        itemView.setBackgroundResource(R.drawable.ac_button_selected);
        ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        viewHolder.paysA.setTextColor(selectedTextColor);
        viewHolder.paysB.setTextColor(selectedTextColor);
        viewHolder.categorie.setTextColor(selectedTextColor);
        viewHolder.tableau.setTextColor(selectedTextColor);
        viewHolder._vs.setTextColor(selectedTextColor);
        viewHolder._separator.setTextColor(selectedTextColor);
    }

    private void unselect(View itemView) {
        itemView.setBackgroundResource(R.drawable.ac_button_normal);
        ViewHolder viewHolder = (ViewHolder) itemView.getTag();
        viewHolder.paysA.setTextColor(normalTextColor);
        viewHolder.paysB.setTextColor(normalTextColor);
        viewHolder.categorie.setTextColor(normalTextColor);
        viewHolder.tableau.setTextColor(normalTextColor);
        viewHolder._vs.setTextColor(normalTextColor);
        viewHolder._separator.setTextColor(normalTextColor);
    }

    public void resetSelection() {
        selectedItem = -1;
        onMatchSelectedListener.onMatchSelected(null);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView drapeauA, drapeauB;
        TextView paysA, paysB;
        TextView categorie;
        TextView tableau;
        TextView _vs;
        TextView _separator;
    }

}
