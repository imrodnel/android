package fr.istic.masteru.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.istic.masteru.android.R;
import fr.istic.masteru.android.activities.MatchSetupActivity;
import fr.istic.masteru.android.utils.Constants;

public class SelectRefereeingTypeFragment extends SetupFragment {

    MatchSetupActivity matchSetupActivity;
    private int lightGrey;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MatchSetupActivity)
            this.matchSetupActivity = (MatchSetupActivity) activity;
        else
            throw new IllegalArgumentException("Activity must be an MatchSetupActivity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        lightGrey = matchSetupActivity.getResources().getColor(R.color.light_grey);
        View root = inflater.inflate(R.layout.select_refereeing_type_fragment, container, false);
        root.findViewById(R.id.previousButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                matchSetupActivity.previous();
            }
        });
        final View nextButton = root.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchSetupActivity.next();
            }
        });
        final View classicDouble = root.findViewById(R.id.classicDouble);
        final View proDouble = root.findViewById(R.id.proDouble);
        classicDouble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classicDouble.setSelected(true);
                proDouble.setSelected(false);
                matchSetupActivity.onRefereeingTypeChange(Constants.DOUBLE_CLASSIC);
                nextButton.setVisibility(View.VISIBLE);
            }
        });
        proDouble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classicDouble.setSelected(false);
                proDouble.setSelected(true);
                matchSetupActivity.onRefereeingTypeChange(Constants.DOUBLE_PRO);
                nextButton.setVisibility(View.VISIBLE);
            }
        });
        return root;
    }

    @Override
    public void cancel() {
        matchSetupActivity.onRefereeingTypeChange(0);
    }
}
